import { ToastController,AlertController,LoadingController, Platform } from 'ionic-angular';
import { FormControl } from "@angular/forms";
import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Storage } from '@ionic/storage';

declare var window: any;
@Injectable()
export class Utilities {

    private BASE_URL="https://mpesa-pesasasa.herokuapp.com";
    private AUTH_TOKEN ="Token 84ea8a129b861838bf13283527d6cb6ccff2ea0e";
    private loader : any;
    public smsList : any;
    public response;

    constructor(
      private toastCtrl: ToastController,
      private loadingCtrl:LoadingController,
      private alertCtrl : AlertController,
      private permissions: AndroidPermissions ,
      private platform: Platform,
      private storage : Storage,
      private http:Http
    ) { }


    getBaseUrl(){
      return this.BASE_URL;
    }

   showMessage(messageIn){
       let toast = this.toastCtrl.create({
        message: messageIn,
        duration: 5000
      });
      toast.present();
   }

   showDialog(title,messageIn){

      let alert = this.alertCtrl.create({
        title: title,
        enableBackdropDismiss:false,
        subTitle: messageIn,
        buttons: ['OK']
      });
      alert.present();
  
   }

   createLoader(messageIn : string){
     this.loader= this.loadingCtrl.create({
          content: messageIn,
        });;
     this.loader.present();
     return this.loader;
   }

   stopLoader(){
      if(this.loader){
         this.loader.dismiss();
      }
   }

   changeLoaderText(text){
      this.loader.setContent(text);
   }


  getDate(numOfDays){
    
  	let d=new Date();
  	d.setDate(d.getDate() + numOfDays);
    let date=d.getFullYear()  + "-" + (d.getMonth()+1) + "-" + d.getDate();
    return date;
  }



  readSMS(){
    if(this.isMobilePlatform()){
        this.askSMSPermission().then(data=>{
          if(window.SMS){
            this.smsList = window.SMS;
          }
        });
    }
  }

  storeObject(key,value){
      this.storage.set(key,value);
  }

  getObject(key){
      return this.storage.get(key);  
  }

  askSMSPermission(){
     return this.permissions.checkPermission(this.permissions.PERMISSION.READ_SMS);
  }

  isMobilePlatform(){
    if (this.platform.is('cordova')) {
      return true;
    }
    return false;
  }

  getDefaultRequestOptions(){
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.AUTH_TOKEN);
      let options = new RequestOptions({headers: headers});
      return options;
  }

  goToPage(page){
    //this.navCtrl.setRoot(page);
  }

  






  

}