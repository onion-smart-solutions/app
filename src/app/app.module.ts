
import { ErrorHandler, NgModule, enableProdMode } from '@angular/core';  //enableProdMode : make development faster
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { MODULES, PROVIDERS, NATIVES ,ENTRYCOMPONENTS } from './app.imports';

// this is the magic wandd
enableProdMode();

@NgModule({
  declarations: [
    MyApp,
    ENTRYCOMPONENTS,
  ],
  imports: [
    MODULES,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ENTRYCOMPONENTS
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PROVIDERS,
    NATIVES,
  ]
})
export class AppModule {}

