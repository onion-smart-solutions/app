import { Component, ViewChild } from '@angular/core';
import { Events, Config, AlertController, Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { AppState, PAGES} from './global.setting';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { SlideWalkthroughPage } from '../pages/slide/slide-walkthrough/slide-walkthrough';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { Utilities } from './app.utils';
import { OneSignal } from '@ionic-native/onesignal';
import { MpesaService } from '../providers/crud-service/mpesa.service';
import { UserService } from '../providers/crud-service/user.service';
import { LoanService } from '../providers/crud-service/loan.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any ;
  user : any ;
  pages:any[];
  activePage = new Subject();
  userData:any;
  isLogin:boolean = false;
  constructor(
    private  platform: Platform,
    private  menu: MenuController,
    private  statusBar: StatusBar,
    private  splashScreen: SplashScreen,
    private  global: AppState,
    private  alertCtrl: AlertController,
    private  menuCtrl: MenuController,
    private  storage:Storage,
    private  utils : Utilities,
    private  mpesaService : MpesaService,
    private  userService : UserService,
    private  loanService : LoanService,
    private _notification: OneSignal

  ) {
    this.initializeApp();
    if(this.platform.is('cordova')){
        this.initPushNotification();
    }
  
    //Main Menu
    this.pages = PAGES;

    this.activePage.subscribe((selectedPage: any) => {
      this.pages.map(page => {
        page.active = page.title === selectedPage.title;
      });
    }); 

    this.userService.getCurrentUser().then(user =>{
      if(user!=null){
          this.setUserData(user);
          this.user =  user ;
      }
    });

   
    this.userData = { 
      'name':'',
      'photo':'',
      'email':'',
    }   
  }



  initializeApp() {
    this.platform.ready().then(() => {
      this.global.set('theme', '');
      this.setMenu(this.MENU.MATERIAL); 
      this.nav.setRoot(LoginPage);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
     
       this.platform.resume.subscribe((result)=>{//Foreground

  		    this.utils.getObject('waiting_mpesa').then(value=>{
	  			if(value==true){
            this.utils.createLoader('Confirming Payment...');
            this.utils.storeObject('waiting_mpesa',false);
            setTimeout(() => 
            {
              this.confirm();
            },
            3000);
	  			}
  		});
      });
    });
  }

  setUserData(data){
    this.userData = {
      name: "Welcome "+data.first_name,
      email: data.email,
      photo: "http://api.adorable.io/avatar/"+data.first_name,
      id : data.id,
    }
  }

  openPage(pages) {
      this.menu.close();
      this.nav.setRoot(pages.page);
      this.activePage.next(pages);
  }

 

  //PUSH NOTIFICATION
  initPushNotification() {
      this._notification.startInit('4292159b-9762-4666-bd76-a17c0a6ed5ba','462605408150');
      this._notification.inFocusDisplaying(this._notification.OSInFocusDisplayOption.Notification);
      this._notification.setSubscription(true);
      this._notification.getIds().then(value=>{
         this.utils.storeObject('notification_id',value.userId);
      });
      this._notification.endInit();
  }


  MENU = {
    DEFAULT: 'menu-components',
    MATERIAL: 'menu-material',
    AVATAR: 'menu-avatar',
  };

  setMenu(menu) {
      Object.keys(this.MENU).map(k => this.menuCtrl.enable(false, this.MENU[k]));
      this.menuCtrl.enable(true, menu);
  }

  exitApp(){
     this.userService.signOutUser();
     this.platform.exitApp();
  }

  signOut(){
    let confirm = this.alertCtrl.create({
      title: 'Sign out and Exit ?',
      message: '',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.exitApp();
          }
        }
      ]
    });
    confirm.present();
    
  }

  confirm(){

      let transaction_details : any;
      this.utils.getObject('transaction_details').then(value=>{
      
      transaction_details = value;
      let response = this.mpesaService.checkTransactionStatus( 
        {
          'CheckoutRequestID' : transaction_details.tracking_id 
        }
      );

      response.timeout(20000)
      .map(res => res.json()).subscribe(data => { 
      this.utils.stopLoader();
      this.utils.storeObject('waiting_mpesa',false);
      console.log(data.ResultCode);
      switch (data.ResultCode) {

        case "0":
            if(transaction_details.reason =='activation'){

                this.utils.createLoader('Activating Account...');
                let user = transaction_details.reference;
                user.status = "Active";
                let response = this.userService.updateUser(user.id,user);
                response.timeout(20000)
                .map(res => res.json()).subscribe(data => {
                    this.userService.storeCurrentUser(user); //sync with local storage
                    this.utils.stopLoader();
                    this.utils.showDialog("Payment Successful",'Your account is now activated.');
                    this.nav.setRoot(HomePage);
                },error=>{
                    this.utils.stopLoader();
                    this.utils.showDialog("Activation Error",error);
                });
            }
            else if(transaction_details.reason == 'loan_settlement'){
                /*Adjust the loan*/
                this.utils.createLoader('Adjusting Loan...');
                let loan = transaction_details.reference;
                loan.loan_balance = loan.loan_balance - transaction_details.amount;
                if(loan.loan_balance == 0){
                    loan.status = "Settled";
                  }

                let response = this.loanService.updateLoan(loan.id,loan);
                response.timeout(20000)
                .map(res => res.json()).subscribe(data => {
                    /*Update user loan limit*/
                    let current_user = this.user;
                    current_user.loan_limit =  current_user.loan_limit * 1.2;
                    let update_response = this.userService.updateUser(current_user.id,current_user);
                    update_response.subscribe(data=>{   
                        this.userService.storeCurrentUser(current_user);              
                        this.utils.stopLoader();
                        this.utils.showDialog("Payment Successful",'Your Loan has been adjusted successfully');
                        this.nav.setRoot(HomePage);
                    });
                },error=>{
                    this.utils.stopLoader();
                    this.utils.showDialog("Loan Repayment Error",error);
                });

              }  
            break;

            case "1":
                this.utils.showDialog('Insufficient Funds','Your Account has insufficient funds.Please top up and try again.');
                break;

            case "1032":
                this.utils.showDialog('Transaction Cancelled','Your cancelled the transaction.');
                break;
            
            default:
                this.utils.showDialog('Payment Response',data.ResultDesc);
                break;
        }

      },error=>{
          this.utils.stopLoader();
          console.log(error);
          this.utils.showMessage('Could not confirm payment');
      });

    });
  }
}
