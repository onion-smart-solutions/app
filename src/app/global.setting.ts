import { HomePage } from '../pages/home/home';
import { LoanListPage } from '../pages/loan/loan-list/loan-list';
import { StatementsPage } from '../pages/statements/statements';
import { ProfilePage } from '../pages/profile/profile';

export const PAGES = [
    { icon: 'apps', title: 'Home', page: HomePage },  
    { icon: 'paper', title: 'My Loans', page: LoanListPage },
    { icon: 'person', title: 'My Profile', page: ProfilePage },
    { icon: 'document', title: 'MPESA Statements', page: StatementsPage }
];

import { Injectable } from '@angular/core';
@Injectable()
export class AppState {
  _state = {};

  // already return a clone of the current state
  get state() {
    return this._state = this.clone(this._state);
  }

  // never allow mutation
  set state(value) {
    throw new Error('do not mutate the `.state` directly');
  }

  get(prop?: any) {
    // use our state getter for the clone
    const state = this.state;
    return state.hasOwnProperty(prop) ? state[prop] : state;
  }

  set(prop: string, value: any) {
    // internally mutate our state
    return this._state[prop] = value;
  }

  private clone(object) {
    // simple object clone
    return JSON.parse(JSON.stringify(object));
  }
}
