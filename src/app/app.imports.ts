//Used for setting
import { ConstantVariable } from './constant-variable';

//used for Firebase
const firebaseConfig = {
    apiKey              : ConstantVariable.apiKey,
    authDomain          : ConstantVariable.authDomain,
    databaseURL         : ConstantVariable.databaseURL,
    projectId           : ConstantVariable.projectId,
    storageBucket       : ConstantVariable.storageBucket,
    messagingSenderId   : ConstantVariable.messagingSenderId
  };

//Used for Theming
import { AppState } from './global.setting';

//MODULE
import { Http, HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserModule } from '@angular/platform-browser';
import { SwingModule } from 'angular2-swing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';

//PROVIDER

import { UserService } from '../providers/crud-service/user.service';
import { LoanService } from '../providers/crud-service/loan.service';
import { SettlementService } from '../providers/crud-service/settlement.service';
import { ActivationService } from '../providers/crud-service/activation.service';
import { StatementService } from '../providers/crud-service/statements.service';
import { Permissions } from '../providers/crud-service/permissions.service';
import { MpesaService } from '../providers/crud-service/mpesa.service';
import { ToastService } from '../providers/util/toast.service';
import { AlertService } from '../providers/util/alert.service';
import { Utilities } from './app.utils';
import { Diagnostic } from '@ionic-native/diagnostic';

/*NATIVES*/

import { SMS } from '@ionic-native/sms';
import { Push } from '@ionic-native/push';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { OneSignal } from '@ionic-native/onesignal';

//ENTRY COMPONENTS

import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { ActivationPage } from '../pages/activation/activation';
import { ProfilePage } from '../pages/profile/profile';
import { LoanApplicationPage } from '../pages/loan/loan-application/loan-application';
import { LoanListPage } from '../pages/loan/loan-list/loan-list';
import { StatementsPage } from '../pages/statements/statements';
import { LoanPaymentModalPage } from '../pages/loan/loan-payment-modal/loan-payment-modal';


export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


export const firebaseConfigEx = firebaseConfig;

export const MODULES = [
    SwingModule,
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
        }
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
];

export const PROVIDERS = [
    AlertService,
    ToastService,
    UserService,
    LoanService,
    ActivationService,
    SettlementService,
    StatementService,
    MpesaService,
    Permissions,
    Utilities,
    Diagnostic,
    AppState,
];

export const NATIVES = [
    SMS,
    Push,
    StatusBar,
    SplashScreen,
    AndroidPermissions,
    OneSignal
];


export const ENTRYCOMPONENTS = [
    RegisterPage,
    LoginPage,
    ActivationPage,
    ProfilePage,
    LoanApplicationPage,
    LoanPaymentModalPage,
    LoanListPage,
    StatementsPage,
    HomePage
];
