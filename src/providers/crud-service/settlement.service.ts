import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Utilities } from '../../app/app.utils';



@Injectable()
export class SettlementService {

    private SETTLEMENTS_URL="/settlements/";
    
    private request_options : any;
    constructor(public utils: Utilities,public http:Http) {
        this.request_options = this.utils.getDefaultRequestOptions();
    }

    getSettlements(loan_id){
        return this.http.get(this.utils.getBaseUrl()+this.SETTLEMENTS_URL+'?loan='+loan_id);     
    }

    createSettlement(data){
        return this.http.post(this.utils.getBaseUrl()+this.SETTLEMENTS_URL, data ,this.request_options);
    }

    updateSettlement(settlement_id,data){
        return this.http.put(this.utils.getBaseUrl()+this.SETTLEMENTS_URL+settlement_id+"/", data , this.request_options);  
    }

  }


