import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Utilities } from '../../app/app.utils';
import { Http,Headers,RequestOptions } from '@angular/http';

@Injectable()
export class MpesaService {

	private MPESA_REQUEST_URL= "/mpesa/stk/process/";
	private MPESA_STATUS_URL = "/mpesa/stk/status/";
    private request_options : any;

    constructor(
        private utils: Utilities,
        private http:Http,
       ) {
        this.request_options = this.utils.getDefaultRequestOptions();
    }

    requestPayment(details){
        return this.http.post(this.utils.getBaseUrl()+this.MPESA_REQUEST_URL,details,this.request_options);
    }

    checkTransactionStatus(transaction_details){
		return this.http.post(this.utils.getBaseUrl()+this.MPESA_STATUS_URL,transaction_details,this.request_options);
    }

}