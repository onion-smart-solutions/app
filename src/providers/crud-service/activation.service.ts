import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Utilities } from '../../app/app.utils';


@Injectable()
export class ActivationService {

    private ACTIVATIONS_URL="/app/activations/";
    private request_options : any;
    constructor(public utils: Utilities,public http:Http) {
        this.request_options = this.utils.getDefaultRequestOptions();
    }

    getActivation(user_id){
       return this.http.get(this.utils.getBaseUrl()+this.ACTIVATIONS_URL+'?user='+user_id);     
    }

    createActivation(data){
        return this.http.post(this.utils.getBaseUrl()+this.ACTIVATIONS_URL, data ,this.request_options);
    }

    updateActivation(id,data){
      return this.http.put(this.utils.getBaseUrl()+this.ACTIVATIONS_URL+id+"/", data , this.request_options);  
    }

}