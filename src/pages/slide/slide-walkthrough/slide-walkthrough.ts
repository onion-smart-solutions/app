import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides } from 'ionic-angular';
import { RegisterPage } from '../../register/register';

@IonicPage()
@Component({
  selector: 'page-slide-walkthrough',
  templateUrl: 'slide-walkthrough.html',
})
export class SlideWalkthroughPage {

  @ViewChild('slider') slider: Slides;
  slideIndex = 0;
  slides = [
    {
      title: 'Introducing Pesa Sasa',
      imageUrl: 'assets/img/lists/wishlist-1.jpg',
      description: 'A digital innovation in Loans',
    },
    {
      title: 'Instant Money to your MPESA',
      imageUrl: 'assets/img/lists/wishlist-2.jpg',
      description: 'Get loans to your MPESA within 24hrs',
    },
    {
      title: 'Sort out your emergencies',
      imageUrl: 'assets/img/lists/wishlist-3.jpg',
      description: 'Dont delay on with payments and bills' ,
    },
    {
      title: 'Get borrowing now',
      imageUrl: 'assets/img/lists/wishlist-4.jpg',
      description: 'Sign Up and get borrowing now',
    }
  ];

  constructor(public navCtrl: NavController) { }

  onSlideChanged() {
    this.slideIndex = this.slider.getActiveIndex();
  }

  goToApp() {
    this.navCtrl.setRoot(RegisterPage);
  }

  skip() {
    this.slider.slideTo(3);
  }
}
