import { Component } from '@angular/core';
import { IonicPage, AlertController,NavController } from 'ionic-angular';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../providers/crud-service/user.service';
import { LoanService } from '../../../providers/crud-service/loan.service';
import { Utilities } from '../../../app/app.utils';
import { StatementsPage } from '../../statements/statements';
import { HomePage } from '../../home/home';
@Component({
  selector: 'page-loan-application',
  templateUrl: 'loan-application.html'
})
export class LoanApplicationPage {
  
  private loanApplicationForm: FormGroup;
  private user : any;
  loan_limit : any;
  constructor(
  	private formBuilder: FormBuilder,
  	private userService: UserService,
  	private loanService: LoanService,
  	private navCtrl : NavController,
  	private alertCtrl : AlertController,
  	private utilities : Utilities) 
  {

	  	this.loanApplicationForm = formBuilder.group({
	        loan_amount: ['', Validators.compose([Validators.maxLength(9), Validators.required])],
	    });

    
	  	
	  
  }

  ionViewDidEnter(){

      this.userService.getCurrentUser().then((user) => {
          if(user!=null){
             this.user = user;
             this.loan_limit = user.loan_limit;
             this.utilities.getObject('mpesa_statements').then((statements)=>{
                if(statements==null){
                    this.promptStatements();
              }
          });
          } 
      });

  }

  requestLoan(details){
  	this.utilities.createLoader('Requesting Loan...');
  	let response = this.loanService.createLoan(details);
    response.timeout(20000)
    .map(res => res.json()).subscribe(data => {
    	this.utilities.stopLoader();
    	this.utilities.showDialog('Loan Request Successful',"Funds will be disbursed shortly to your M-PESA");
      this.navCtrl.setRoot(HomePage);
    },error=>{
    	this.utilities.stopLoader();
    	this.utilities.showDialog("Error Requesting Loan",error);
    });
	  	
  }

  confirm() {

  	  let loan_amount = this.loanApplicationForm.controls['loan_amount'].value;
	  	if(loan_amount > this.user.loan_limit){
	  		this.utilities.showDialog('Loan Limit Error',"Your Loan request has exceeded your limit");
	  	}

	  	else{

		  	let loan_details={ 
	           'user' : this.user.id,
	           'applicationDate' : this.utilities.getDate(0),
	           'dueDate' : this.utilities.getDate(30),
	           'loan_amount' : loan_amount,
	           'status' : 'Pending',
	           'loan_balance' : loan_amount * 1.2
	        };

			let confirm = this.alertCtrl.create({
			title: 'Confirm Loan Request',
			message:
			       '<strong> Amount Requested :</strong> Kshs. '+loan_details['loan_amount']+'<br>'+
			       '<strong> Amount to Repay :</strong> Kshs. '+loan_details['loan_balance']+'<br>'+
			       '<strong> Application Date:</strong>  '+loan_details['applicationDate']+'<br>'+
			       '<strong> Due Date :</strong>  '+loan_details['dueDate']+'<br>',
			buttons: [
				{
				  text: 'Cancel',

				},
				{
				  text: 'Proceed',
				  handler: () => {
				     this.requestLoan(loan_details);
				  }
				}
			]
	    });
	    confirm.present();
	 }

  }

  promptStatements(){
    let confirm = this.alertCtrl.create({
      title: 'Upload your statements',
      message: 'You cannot apply for a new loan because you have not uploaded your MPESA statements. Upload Now',
      enableBackdropDismiss:false,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
             this.navCtrl.setRoot(HomePage);
          }
        },
        {
          text: 'Upload Now',
          handler: () => {
             this.navCtrl.push(StatementsPage);
          }
        }
      ]
    });
    confirm.present();
  }

}
