import { Component } from '@angular/core';
import { NavController,AlertController,Platform } from 'ionic-angular';
import { Utilities } from '../../../app/app.utils';
import { Http } from '@angular/http';
import { UserService } from '../../../providers/crud-service/user.service';
import { LoanService } from '../../../providers/crud-service/loan.service';
import { StatementService } from '../../../providers/crud-service/statements.service';
import { LoanApplicationPage } from '../../loan/loan-application/loan-application';
import { LoanPaymentModalPage } from '../../loan/loan-payment-modal/loan-payment-modal';

@Component({
  selector: 'page-loan-list',
  templateUrl: 'loan-list.html'
})
export class LoanListPage {
  loans = null;
  statements = null;
  user : any;
  constructor(
              private  navCtrl : NavController,
              private  alertCtrl : AlertController,
              private  userService : UserService,
              private  statementService : StatementService,
              private  loanService : LoanService,
              private  utils : Utilities,
           ) 
  {
     
  }

  ionViewDidEnter(){
      this.userService.getCurrentUser().then((user) => {
        if(user!=null){
           this.user =  user;
           this.getUserLoans(user.id);
        }
      },error=>{
          this.utils.showMessage(error);
      });
  }

  applyLoan(){
      if(this.loans!=null){
        if(this.checkEligibility()){
           this.navCtrl.push(LoanApplicationPage, {}, {animate: true, direction: 'forward'});
        }
        else
        {
          this.utils.showDialog('Not Eligible','You cannot apply for a new loan because you have unsettled or pending loans');
        }
      }
   
      else{
           this.navCtrl.push(LoanApplicationPage, {}, {animate: true, direction: 'forward'});   
      }
     
  }

  getUserLoans(user_id){
    this.utils.createLoader('Loading...');
    let response = this.loanService.getLoans(user_id);
    response.timeout(20000).map(res => res.json()).subscribe(data => {
        this.utils.stopLoader();
        var length = Object.keys(data).length;
          if(length>0){
             this.loans = data;
        }
      }
    );
  }


  makePayment(loan){
      this.navCtrl.push(LoanPaymentModalPage, loan);
  }


  checkEligibility(){
    for (var i = 0; i < this.loans.length; i++) { 
       if (this.loans[i].status=='Pending'||this.loans[i].status=='Active') {
          return false;
       }
    }
    return true;
  }

  doRefresh(refresher) {
    setTimeout(() => {    
      this.getUserLoans(this.user.id);
      refresher.complete();
    }, 2000);
  }
   
}
