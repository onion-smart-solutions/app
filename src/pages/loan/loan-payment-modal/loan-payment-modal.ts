import { Component } from '@angular/core';
import { NavController,AlertController,LoadingController,NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SettlementService } from '../../../providers/crud-service/settlement.service';
import { UserService } from '../../../providers/crud-service/user.service';
import { MpesaService } from '../../../providers/crud-service/mpesa.service';
import { Utilities } from '../../../app/app.utils';
import { ConstantVariable } from '../../../app/constant-variable';

@Component({
  selector: 'page-loan-payment-modal',
  templateUrl: 'loan-payment-modal.html'
})
export class LoanPaymentModalPage {
  
  private loanpaymentModalForm: FormGroup;
  private user : any;
  private loan : any;
  constructor(
  	private formBuilder: FormBuilder,
    private navParams : NavParams,
    private userService : UserService,
    private mpesaService : MpesaService,
    private alertCtrl : AlertController,
    private navCtrl : NavController,
    private utils : Utilities,
    private settlementService : SettlementService,
) 
  {
	  	this.loanpaymentModalForm = formBuilder.group({
	        amount: ['', Validators.compose([Validators.maxLength(9), Validators.required])],
	    });

    	this.userService.getCurrentUser().then((user) => {
	        if(user!=null){
	           this.user = user;
	        } 
	    });

      this.loan = navParams.get('loanIn');
  }


  confirm() {

  	  let amount = this.loanpaymentModalForm.controls['amount'].value;
	  	if(amount > this.loan.loan_balance){
	  		this.utils.showDialog('Amount exceeds Balance',"You cannot pay more than you owe !");
	  	}

	  	else{

  			let confirm = this.alertCtrl.create({
  			title: 'Confirm Loan Repayment',
  			message:
  			       '<strong> Amount to Pay :</strong> Kshs. '+amount+'<br>'+
  			       '<strong> Balance after payment :</strong> Kshs. '+(this.loan.loan_balance - amount)+'<br>'+
  			       '<strong> Due Date :</strong>  '+this.loan.dueDate+'<br>',
  			buttons: [
  				{
  				  text: 'Cancel',
  				},
  				{
  				  text: 'Submit',
  				  handler: () => {
  				     this.inititatePayment();
  				  }
  				}
  			]
  	    });
  	    confirm.present();
	 }

  }

  goBack(){
    this.navCtrl.pop();
  }

  inititatePayment(){

    this.utils.createLoader('Initiating Payment');
      let phone_no = this.user.phone_no.substr(1);
      let paymentDetails=
      { 
          'amount': this.loanpaymentModalForm.controls['amount'].value,
          'phone': "254"+phone_no,
          'reference': "PESA000"+this.user.id,
          'desc' : "Loan Settlement",
      };

      let response = this.mpesaService.requestPayment(paymentDetails);
      response.timeout(40000)
      .map(res => res.json()).subscribe(data => {  
          this.utils.stopLoader();
          if(data.ResponseCode == 0){
            let transaction_details = 
            {
                'amount': paymentDetails['amount'],
                'tracking_id': data.CheckoutRequestID,
                'reference': this.loan,
                'reason' : "loan_settlement",
            }
            this.utils.storeObject('waiting_mpesa',true);
            this.utils.storeObject('transaction_details',transaction_details);
          }
          else{
             this.utils.showDialog('MPESA Response',data.ResponseDescription);
          }
        },error =>{
            this.utils.stopLoader();
            this.utils.showMessage(error);
        }
      );
  }

  

}
