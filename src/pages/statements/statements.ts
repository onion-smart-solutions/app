import { Component } from '@angular/core';
import { NavController,AlertController,Platform } from 'ionic-angular';
import { Utilities } from '../../app/app.utils';
import { Http } from '@angular/http';
import { UserService } from '../../providers/crud-service/user.service';
import { LoanService } from '../../providers/crud-service/loan.service';
import { StatementService } from '../../providers/crud-service/statements.service';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Permissions} from '../../providers/crud-service/permissions.service';
import { Diagnostic } from '@ionic-native/diagnostic';


declare var window: any;

@Component({
  selector: 'statements',
  templateUrl: 'statements.html'
})

export class StatementsPage {
  statements : any;
  user : any;
  constructor(
    private  navCtrl : NavController,
    private  diagnostic: Diagnostic,
    private  permissions: Permissions,
    private androidPermissions: AndroidPermissions,
    private  statementService : StatementService,
    private  userService : UserService,
    private  utils : Utilities) 
  {
      this.userService.getCurrentUser().then(user=>{
           this.user = user;
      }); 
      if(this.utils.isMobilePlatform()){
          this.checkPermissions();
      }


  }

   getSMS(){
     var filter = {
            address : 'MPESA', 
            indexFrom : 0, // start from index 0
            maxCount : 50, // count of SMS to return each time
          };
    if(window.SMS) window.SMS.listSMS(filter,data=>{
        setTimeout(()=>{
            this.statements=data;
        },30)
 
    },error=>{
      console.log(error);
    });
  }

  uploadStatement(){
    let current_user = this.user;
    if(this.statements!=null){
       this.utils.storeObject('mpesa_statements',this.statements);
       let counter = 1;
       let length = this.statements.length;
       let that = this;
       this.utils.createLoader('Uploading Statements...');
       this.statements.forEach(function(statement) 
       { 
          let statementDetails = {
            "user" : current_user.id,
            "details" : statement.body,
          };
          that.statementService.createStatement(statementDetails).subscribe(data=>{
              if(counter==length){
                 that.utils.stopLoader();
                 that.utils.showMessage('Upload Successful');
              }
              else{
                 counter++;
              } 
          });
       });  
    }
    else{
      this.utils.showMessage('No statements found');
    }
  }


  checkPermissions(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
        success => this.getSMS(),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
    this.getSMS();
  }

  doRefresh(refresher) {
    setTimeout(() => {    
      if(this.utils.isMobilePlatform()){
          this.checkPermissions();
      }
      refresher.complete();
    }, 3000);
  }
      
    
       
}