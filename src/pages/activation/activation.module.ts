import { ActivationPage } from './activation';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    ActivationPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivationPage),
  ],
  exports: [
    ActivationPage
  ]
})

export class ActivationPageModule { }
