import { Component } from '@angular/core';
import { AlertController,NavController, IonicPage } from 'ionic-angular';
import { MpesaService } from '../../providers/crud-service/mpesa.service';
import { UserService } from '../../providers/crud-service/user.service';
import { Utilities } from '../../app/app.utils';
import { ConstantVariable } from '../../app/constant-variable';
@IonicPage()
@Component({
  selector: 'page-activation',
  templateUrl: 'activation.html'
})
export class ActivationPage {
  
  private user : any;
  constructor(public navCtrl: NavController,
  	public alertCtrl :AlertController,
  	public mpesaService : MpesaService,
  	public utils : Utilities,
  	) {

  	this.utils.getObject('user').then(value=>{
         this.user = value ;
  	});
	
  }

  activate(){

  		this.utils.createLoader('Initiating Payment');
	      let phone_no = this.user.phone_no.substr(1);
		  	let paymentDetails=
		    { 
	  	      'amount': ConstantVariable.ACTIVATION_FEE,
	  	      'phone': "254"+phone_no,
	  	      'reference': "PESA000"+this.user.id,
	  	      'desc' : "Activation Fee",
	  	  };

	    let response = this.mpesaService.requestPayment(paymentDetails);
	    response.timeout(40000)
	    .map(res => res.json()).subscribe(data => {  
          this.utils.stopLoader();
	        if(data.ResponseCode == 0){
            let transaction_details = 
            {
                'amount': ConstantVariable.ACTIVATION_FEE,
                'tracking_id': data.CheckoutRequestID,
                'reference':this.user,
                'reason' : "activation",
            }
            this.utils.storeObject('waiting_mpesa',true);
            this.utils.storeObject('transaction_details',transaction_details);
	        }
          else{
             this.utils.showDialog('Pesa Sasa',data.ResponseDescription);
          }
	      },error =>{
	      	  this.utils.stopLoader();
	      		this.utils.showMessage(error);
	      }
	    );

  }

 
}
