import { Component } from '@angular/core';
import { NavController,AlertController, IonicPage,Platform } from 'ionic-angular';
import { UserService } from '../../providers/crud-service/user.service';
import { Utilities } from '../../app/app.utils';
import { PAGES} from '../../app/global.setting';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages: Array<{icon :string,title: string, page: any}>;
  
  cards = [
    {
      content: 'Repay your loans on time to increase your limit.',
    },
    {
      content: 'Ensure your MPESA statements are uploaded',
    },
    {
      content: 'Borrow as little as Ksh.100',
    }
  ]

  constructor(
      private navCtrl: NavController,
      private alertCtrl : AlertController,
      private platform : Platform,
  		private utils: Utilities) {  
        this.pages =  PAGES;
  }


  showList(pages) {   
    this.navCtrl.setRoot(pages.page);
  }

  promptActivation(){
    let confirm = this.alertCtrl.create({
      title: 'Contact Admin',
      message: 'Your Account has been blocked.',
      enableBackdropDismiss:false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    confirm.present();
  }
}
