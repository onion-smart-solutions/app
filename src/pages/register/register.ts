import { Component,ViewChild } from '@angular/core';
import { App,NavController, IonicPage } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { ActivationPage } from '../activation/activation';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Utilities } from '../../app/app.utils';
import { UserService } from '../../providers/crud-service/user.service';
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild('signupSlider') signupSlider: any;
  public signUpOneForm: FormGroup;
  public signUpTwoForm: FormGroup;
  public signUpThreeForm: FormGroup;
  public signUpFourForm: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    public utils: Utilities,
    private userService: UserService,
    private navCtrl: NavController,
    public app: App
  ) { 

    this.signUpOneForm = formBuilder.group({
        first_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
        last_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
    });

    this.signUpTwoForm = formBuilder.group({
        email: ['', Validators.compose([Validators.email, Validators.required])],
        national_id: ['', Validators.compose([Validators.maxLength(12), Validators.pattern('[0-9]+'), Validators.required])] 
    });


    this.signUpThreeForm = formBuilder.group({
        phone: ['', Validators.required],
        residence: ['', Validators.required],
    });

     this.signUpFourForm = formBuilder.group({
        password: ['', Validators.compose([Validators.minLength(6),Validators.required])],
        confirm_password: ['', Validators.compose([
               Validators.required,
               this.isEqualPassword.bind(this)
        ])],
    });

  }

  isEqualPassword(control: FormControl): {[s: string]: boolean} {
       if (!this.signUpFourForm) {
           return {passwordsNotMatch: true};
       }
       if (control.value !== this.signUpFourForm.controls['password'].value) {
           return {passwordsNotMatch: true};
       }
   }


  register() {
    
    this.utils.createLoader('Finishing registration...')

    let UserDetails = {
        'first_name': this.signUpOneForm.controls['first_name'].value,
        'last_name': this.signUpOneForm.controls['last_name'].value,
        'email': this.signUpTwoForm.controls['email'].value,
        'national_id': this.signUpTwoForm.controls['national_id'].value,
        'phone_no': this.signUpThreeForm.controls['phone'].value,
        'residence': this.signUpThreeForm.controls['residence'].value,
        'password': this.signUpFourForm.controls['password'].value,
        'status' : 'Pending'
    };

    let response = this.userService.createUser(UserDetails);
    response.timeout(40000)
    .map(res => res.json()).subscribe(data => {
        
      this.userService.storeCurrentUser(data);

      /*Add user to firebase service*/
      let fireBaseUserDetails = {
            'email':  this.signUpTwoForm.controls['email'].value,
            'name': this.signUpOneForm.controls['first_name'].value+' '+this.signUpOneForm.controls['last_name'].value,
            'password': this.signUpFourForm.controls['password'].value
      };
     
      let firebaseResponse = this.userService.createUserFB(fireBaseUserDetails);
      firebaseResponse.then(auth => {
          this.utils.stopLoader();               
          this.navCtrl.setRoot(ActivationPage);
      }).catch(err => {
           this.utils.stopLoader();
           this.utils.showDialog('Sign Up Error',err.message);
      });

      },error=>{
        this.utils.stopLoader();
        this.utils.showMessage(error);
      }
    );

  }

  goToSignin() {
     this.navCtrl.setRoot(LoginPage);
  }

  next(index : any,form : FormGroup){
    if(form.valid){
        this.signupSlider.slideTo(index);
    }
  }
  prev(index){
     this.signupSlider.slideTo(index);
  }


}
