// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component } from '@angular/core';
import { AlertController, App, LoadingController,NavController, IonicPage } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { ActivationPage } from '../activation/activation';
import { Utilities } from '../../app/app.utils';
import { UserService } from '../../providers/crud-service/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public loginForm: any;
  public user : any;
  
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public userService: UserService,
    public utils :Utilities,
    public app: App,
  ) {
        this.userService.getCurrentUser().then((value) => {
          if(value!=null){
              this.user = value;
              this.loginForm.controls['email'].setValue(value.email);
          } 
        }); 

        this.loginForm = formBuilder.group({
            email: ['',Validators.compose([Validators.email, Validators.required])],
            password: ['', Validators.required]
        });
    }

  login() {
 
     /*Firebase Login*/

     this.utils.createLoader('Signing in...');
      let details=
      { 
         'email': this.loginForm.controls['email'].value,
         'password': this.loginForm.controls['password'].value
      };

      let response=this.userService.getUserByEmail(details.email);
      response.timeout(20000)
      .map(res => res.json()).subscribe(data => {
          var length = Object.keys(data).length;
          if(length>0){
              let user = data[0];
              //this.validateUserWithDevice(user);
              this.userService.signInUserFB(details)
                  .then(auth => {
                      this.utils.stopLoader();
                      this.userService.storeCurrentUser(user);
                      if(user.status == "Active"){
                         this.navCtrl.setRoot(HomePage);
                      }
                      else if(user.status == "Pending"){
                         this.navCtrl.setRoot(ActivationPage);
                      }
                      else{
                        this.utils.showDialog('Account Blocked','Your Account was blocked.Contact Support');
                      }
                      
                  })
                  .catch(err => {
                     this.utils.stopLoader();
                     this.utils.showMessage(err.message);
                  });
          }
          else{
            this.utils.stopLoader();
            this.utils.showMessage('User with that email does not exist');
          }
        },error => {
           this.utils.stopLoader();
           this.utils.showMessage(error);
      });

  }

  goToSignup() {
     this.navCtrl.setRoot(RegisterPage);
  }

  forgot(){
    let prompt = this.alertCtrl.create({
      title: 'Password Reset',
      message: "Please enter your email address. We will send you an email to reset your password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email Address'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
        },
        {
          text: 'Send',
          handler: data => {
          this.userService.resetPassword(data.email).then(auth => {
              this.utils.showDialog('Password Reset Successful','A password reset link was sent to your email');
          })
         .catch(err => {
            this.utils.showMessage(err.message);
         });
            
          }
        }
      ]
    });
    prompt.present();
  
   }

   validateUserWithDevice(user){
      /*Get Device Info*/
      this.utils.getObject('sim_info').then((value) => {
          if(value!=null){
            if(value.deviceId==user.device_id){

                  let details=
                  { 
                     'email': this.loginForm.controls['email'].value,
                     'password': this.loginForm.controls['password'].value
                  };

                  this.userService.signInUserFB(details)
                  .then(auth => {
                      this.utils.stopLoader();
                      this.userService.storeCurrentUser(user);
                     // this.navCtrl.setRoot(HomePage);
                  })
                  .catch(err => {
                     this.utils.stopLoader();
                     this.utils.showMessage(err.message);
                  });

              }
            else{
                this.utils.stopLoader();
                this.utils.showMessage('The Mobile Device does not match with your account details');
              }
        }
      });
   }

}
