webpackJsonp([7],{

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatementService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StatementService = /** @class */ (function () {
    function StatementService(http, utils) {
        this.http = http;
        this.utils = utils;
        this.STATEMENT_URL = "/app/statements/";
        this.request_options = this.utils.getDefaultRequestOptions();
    }
    StatementService.prototype.createStatement = function (data) {
        return this.http.post(this.utils.getBaseUrl() + this.STATEMENT_URL, data, this.request_options);
    };
    StatementService.prototype.getStatement = function (user_id) {
        return this.http.get(this.utils.getBaseUrl() + this.STATEMENT_URL + '?user=' + user_id);
    };
    StatementService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */]])
    ], StatementService);
    return StatementService;
}());

//# sourceMappingURL=statements.service.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatementsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_statements_service__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_crud_service_permissions_service__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_diagnostic__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var StatementsPage = /** @class */ (function () {
    function StatementsPage(navCtrl, diagnostic, permissions, androidPermissions, statementService, userService, utils) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.diagnostic = diagnostic;
        this.permissions = permissions;
        this.androidPermissions = androidPermissions;
        this.statementService = statementService;
        this.userService = userService;
        this.utils = utils;
        this.userService.getCurrentUser().then(function (user) {
            _this.user = user;
        });
        if (this.utils.isMobilePlatform()) {
            this.checkPermissions();
        }
    }
    StatementsPage.prototype.getSMS = function () {
        var _this = this;
        var filter = {
            address: 'MPESA',
            indexFrom: 0,
            maxCount: 50,
        };
        if (window.SMS)
            window.SMS.listSMS(filter, function (data) {
                setTimeout(function () {
                    _this.statements = data;
                }, 30);
            }, function (error) {
                console.log(error);
            });
    };
    StatementsPage.prototype.uploadStatement = function () {
        var current_user = this.user;
        if (this.statements != null) {
            this.utils.storeObject('mpesa_statements', this.statements);
            var counter_1 = 1;
            var length_1 = this.statements.length;
            var that_1 = this;
            this.utils.createLoader('Uploading Statements...');
            this.statements.forEach(function (statement) {
                var statementDetails = {
                    "user": current_user.id,
                    "details": statement.body,
                };
                that_1.statementService.createStatement(statementDetails).subscribe(function (data) {
                    if (counter_1 == length_1) {
                        that_1.utils.stopLoader();
                        that_1.utils.showMessage('Upload Successful');
                    }
                    else {
                        counter_1++;
                    }
                });
            });
        }
        else {
            this.utils.showMessage('No statements found');
        }
    };
    StatementsPage.prototype.checkPermissions = function () {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(function (success) { return _this.getSMS(); }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.READ_SMS); });
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
        this.getSMS();
    };
    StatementsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            if (_this.utils.isMobilePlatform()) {
                _this.checkPermissions();
            }
            refresher.complete();
        }, 3000);
    };
    StatementsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'statements',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\statements\statements.html"*/'<ion-header>\n    <ion-navbar color="primary">\n    <button ion-button menuToggle icon-only>\n      <ion-icon name=\'menu\'></ion-icon>\n    </button>\n    <ion-title>\n      MPESA STATEMENTS\n    </ion-title>\n\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n\n   <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <ion-label text-center padding margin *ngIf="statements==null">\n    No M-PESA transactions detected <br> on your phone.\n  </ion-label>\n\n\n\n\n<ion-card *ngFor="let statement of statements">\n  <ion-card-header  color="primary">\n   {{statement.address}}\n  </ion-card-header>\n  <ion-card-content>\n     <p>{{statement.body}} </p>\n  </ion-card-content>\n  </ion-card>\n\n  <ion-fab bottom right>\n    <button  (click)="uploadStatement()" ion-fab><ion-icon name="cloud-upload"></ion-icon></button>\n  </ion-fab>\n\n</ion-content>\n\n\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\statements\statements.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_6__providers_crud_service_permissions_service__["a" /* Permissions */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_statements_service__["a" /* StatementService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */]])
    ], StatementsPage);
    return StatementsPage;
}());

//# sourceMappingURL=statements.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__activation_activation__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { FormBuilder, FormControl, Validator } from '@angular/forms';








var LoginPage = /** @class */ (function () {
    function LoginPage(loadingCtrl, alertCtrl, navCtrl, formBuilder, userService, utils, app) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.utils = utils;
        this.app = app;
        this.userService.getCurrentUser().then(function (value) {
            if (value != null) {
                _this.user = value;
                _this.loginForm.controls['email'].setValue(value.email);
            }
        });
        this.loginForm = formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required])],
            password: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required]
        });
    }
    LoginPage.prototype.login = function () {
        /*Firebase Login*/
        var _this = this;
        this.utils.createLoader('Signing in...');
        var details = {
            'email': this.loginForm.controls['email'].value,
            'password': this.loginForm.controls['password'].value
        };
        var response = this.userService.getUserByEmail(details.email);
        response.timeout(20000)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            var length = Object.keys(data).length;
            if (length > 0) {
                var user_1 = data[0];
                //this.validateUserWithDevice(user);
                _this.userService.signInUserFB(details)
                    .then(function (auth) {
                    _this.utils.stopLoader();
                    _this.userService.storeCurrentUser(user_1);
                    if (user_1.status == "Active") {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
                    }
                    else if (user_1.status == "Pending") {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__activation_activation__["a" /* ActivationPage */]);
                    }
                    else {
                        _this.utils.showDialog('Account Blocked', 'Your Account was blocked.Contact Support');
                    }
                })
                    .catch(function (err) {
                    _this.utils.stopLoader();
                    _this.utils.showMessage(err.message);
                });
            }
            else {
                _this.utils.stopLoader();
                _this.utils.showMessage('User with that email does not exist');
            }
        }, function (error) {
            _this.utils.stopLoader();
            _this.utils.showMessage(error);
        });
    };
    LoginPage.prototype.goToSignup = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.forgot = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Password Reset',
            message: "Please enter your email address. We will send you an email to reset your password.",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email Address'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        _this.userService.resetPassword(data.email).then(function (auth) {
                            _this.utils.showDialog('Password Reset Successful', 'A password reset link was sent to your email');
                        })
                            .catch(function (err) {
                            _this.utils.showMessage(err.message);
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.validateUserWithDevice = function (user) {
        var _this = this;
        /*Get Device Info*/
        this.utils.getObject('sim_info').then(function (value) {
            if (value != null) {
                if (value.deviceId == user.device_id) {
                    var details = {
                        'email': _this.loginForm.controls['email'].value,
                        'password': _this.loginForm.controls['password'].value
                    };
                    _this.userService.signInUserFB(details)
                        .then(function (auth) {
                        _this.utils.stopLoader();
                        _this.userService.storeCurrentUser(user);
                        // this.navCtrl.setRoot(HomePage);
                    })
                        .catch(function (err) {
                        _this.utils.stopLoader();
                        _this.utils.showMessage(err.message);
                    });
                }
                else {
                    _this.utils.stopLoader();
                    _this.utils.showMessage('The Mobile Device does not match with your account details');
                }
            }
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\login\login.html"*/'<ion-content padding class="transparent-header">\n  <ion-header>\n    <ion-navbar>\n    </ion-navbar>\n  </ion-header>\n  <img class="logo" src="assets/img/logo/logo-instagram.png" />\n  <form [formGroup]="loginForm">\n  <div padding> \n    <ion-item [class.invalid]="!loginForm.controls.email.valid  && (loginForm.controls.email.touched  || submitAttempt)">\n      <ion-input type="email" formControlName="email" placeholder="Email"></ion-input>\n    </ion-item>\n\n    <ion-item [class.invalid]="!loginForm.controls.password.valid  && (loginForm.controls.password.touched  || submitAttempt)">\n      <ion-input type="password" formControlName="password" placeholder="Password"></ion-input>\n    </ion-item>\n\n    <button  type="submit" [disabled]="!loginForm.valid" ion-button block outline (click)="login()" color="light" class="login-button">Login</button>\n\n    <ion-label (click)="forgot()" color="light" padding text-center>\n     Forgot Password ?\n    </ion-label>\n  </div>\n</form>\n</ion-content>\n<ion-footer>\n  <ion-toolbar class="footer">\n    <div (click)="goToSignup()" >\n      <span>Don\'t have an account? <strong>Sign up here</strong>.</span>\n    </div>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_6__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_5__app_app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utilities; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Utilities = /** @class */ (function () {
    function Utilities(toastCtrl, loadingCtrl, alertCtrl, permissions, platform, storage, http) {
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.permissions = permissions;
        this.platform = platform;
        this.storage = storage;
        this.http = http;
        this.BASE_URL = "https://mpesa-pesasasa.herokuapp.com";
        this.AUTH_TOKEN = "Token 84ea8a129b861838bf13283527d6cb6ccff2ea0e";
    }
    Utilities.prototype.getBaseUrl = function () {
        return this.BASE_URL;
    };
    Utilities.prototype.showMessage = function (messageIn) {
        var toast = this.toastCtrl.create({
            message: messageIn,
            duration: 5000
        });
        toast.present();
    };
    Utilities.prototype.showDialog = function (title, messageIn) {
        var alert = this.alertCtrl.create({
            title: title,
            enableBackdropDismiss: false,
            subTitle: messageIn,
            buttons: ['OK']
        });
        alert.present();
    };
    Utilities.prototype.createLoader = function (messageIn) {
        this.loader = this.loadingCtrl.create({
            content: messageIn,
        });
        ;
        this.loader.present();
        return this.loader;
    };
    Utilities.prototype.stopLoader = function () {
        if (this.loader) {
            this.loader.dismiss();
        }
    };
    Utilities.prototype.changeLoaderText = function (text) {
        this.loader.setContent(text);
    };
    Utilities.prototype.getDate = function (numOfDays) {
        var d = new Date();
        d.setDate(d.getDate() + numOfDays);
        var date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
        return date;
    };
    Utilities.prototype.readSMS = function () {
        var _this = this;
        if (this.isMobilePlatform()) {
            this.askSMSPermission().then(function (data) {
                if (window.SMS) {
                    _this.smsList = window.SMS;
                }
            });
        }
    };
    Utilities.prototype.storeObject = function (key, value) {
        this.storage.set(key, value);
    };
    Utilities.prototype.getObject = function (key) {
        return this.storage.get(key);
    };
    Utilities.prototype.askSMSPermission = function () {
        return this.permissions.checkPermission(this.permissions.PERMISSION.READ_SMS);
    };
    Utilities.prototype.isMobilePlatform = function () {
        if (this.platform.is('cordova')) {
            return true;
        }
        return false;
    };
    Utilities.prototype.getDefaultRequestOptions = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.AUTH_TOKEN);
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return options;
    };
    Utilities.prototype.goToPage = function (page) {
        //this.navCtrl.setRoot(page);
    };
    Utilities = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["n" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], Utilities);
    return Utilities;
}());

//# sourceMappingURL=app.utils.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__activation_activation__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_crud_service_user_service__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RegisterPage = /** @class */ (function () {
    function RegisterPage(formBuilder, utils, userService, navCtrl, app) {
        this.formBuilder = formBuilder;
        this.utils = utils;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.app = app;
        this.signUpOneForm = formBuilder.group({
            first_name: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(30), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            last_name: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(30), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
        });
        this.signUpTwoForm = formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            national_id: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(12), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].pattern('[0-9]+'), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])]
        });
        this.signUpThreeForm = formBuilder.group({
            phone: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            residence: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
        });
        this.signUpFourForm = formBuilder.group({
            password: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(6), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            confirm_password: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required,
                    this.isEqualPassword.bind(this)
                ])],
        });
    }
    RegisterPage.prototype.isEqualPassword = function (control) {
        if (!this.signUpFourForm) {
            return { passwordsNotMatch: true };
        }
        if (control.value !== this.signUpFourForm.controls['password'].value) {
            return { passwordsNotMatch: true };
        }
    };
    RegisterPage.prototype.register = function () {
        var _this = this;
        this.utils.createLoader('Finishing registration...');
        var UserDetails = {
            'first_name': this.signUpOneForm.controls['first_name'].value,
            'last_name': this.signUpOneForm.controls['last_name'].value,
            'email': this.signUpTwoForm.controls['email'].value,
            'national_id': this.signUpTwoForm.controls['national_id'].value,
            'phone_no': this.signUpThreeForm.controls['phone'].value,
            'residence': this.signUpThreeForm.controls['residence'].value,
            'password': this.signUpFourForm.controls['password'].value,
            'status': 'Pending'
        };
        var response = this.userService.createUser(UserDetails);
        response.timeout(40000)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.userService.storeCurrentUser(data);
            /*Add user to firebase service*/
            var fireBaseUserDetails = {
                'email': _this.signUpTwoForm.controls['email'].value,
                'name': _this.signUpOneForm.controls['first_name'].value + ' ' + _this.signUpOneForm.controls['last_name'].value,
                'password': _this.signUpFourForm.controls['password'].value
            };
            var firebaseResponse = _this.userService.createUserFB(fireBaseUserDetails);
            firebaseResponse.then(function (auth) {
                _this.utils.stopLoader();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__activation_activation__["a" /* ActivationPage */]);
            }).catch(function (err) {
                _this.utils.stopLoader();
                _this.utils.showDialog('Sign Up Error', err.message);
            });
        }, function (error) {
            _this.utils.stopLoader();
            _this.utils.showMessage(error);
        });
    };
    RegisterPage.prototype.goToSignin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    RegisterPage.prototype.next = function (index, form) {
        if (form.valid) {
            this.signupSlider.slideTo(index);
        }
    };
    RegisterPage.prototype.prev = function (index) {
        this.signupSlider.slideTo(index);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('signupSlider'),
        __metadata("design:type", Object)
    ], RegisterPage.prototype, "signupSlider", void 0);
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\register\register.html"*/'<ion-content padding class="transparent-header">\n  <ion-header>\n    <ion-navbar>\n    </ion-navbar>\n  </ion-header>\n  <img class="logo" src="assets/img/logo/logo-instagram.png" />\n\n  <div padding>\n<ion-slides #signupSlider>\n \n  <ion-slide>\n \n     <form [formGroup]="signUpOneForm">\n      <ion-item [class.invalid]="!signUpOneForm.controls.first_name.valid  && (signUpOneForm.controls.first_name.touched  || submitAttempt)">\n        <ion-input  type="text" formControlName="first_name" placeholder="First Name"></ion-input>\n      </ion-item>\n\n\n\n      <ion-item [class.invalid]="!signUpOneForm.controls.last_name.valid  && (signUpOneForm.controls.last_name.touched  || submitAttempt)">\n        <ion-input type="text" formControlName="last_name" placeholder="Last Name"></ion-input>\n      </ion-item>\n\n\n        <button [disabled]="!signUpOneForm.valid" type="submit" ion-button block outline (click)="next(1,signUpOneForm)" color="light" class="login-button">NEXT</button>\n\n    </form>\n\n  </ion-slide>\n\n  <ion-slide>\n  \n     <form [formGroup]="signUpTwoForm">\n\n      <ion-item [class.invalid]="!signUpTwoForm.controls.email.valid  && (signUpTwoForm.controls.email.touched  || submitAttempt)">\n        <ion-input type="email" formControlName="email" placeholder="Email"></ion-input>\n      </ion-item>\n\n\n      <ion-item [class.invalid]="!signUpTwoForm.controls.national_id.valid  && (signUpTwoForm.controls.national_id.touched  || submitAttempt)">\n        <ion-input type="number" formControlName="national_id" placeholder="National ID"></ion-input>\n      </ion-item>\n\n        <ion-row>\n          <ion-col>\n               <button ion-button block outline (click)="prev(0)" color="light" class="login-button">BACK</button>\n          </ion-col>\n\n          <ion-col>\n            <button [disabled]="!signUpTwoForm.valid" type="submit" ion-button block outline (click)="next(2,signUpTwoForm)" color="light" class="login-button">NEXT</button>\n          </ion-col>\n\n         </ion-row>\n     </form>\n\n  </ion-slide>\n\n\n  <ion-slide>\n     \n      <form  [formGroup]="signUpThreeForm">\n      <ion-item [class.invalid]="!signUpThreeForm.controls.phone.valid  && (signUpThreeForm.controls.phone.touched  || submitAttempt)">\n        <ion-input type="text" formControlName="phone" placeholder="Phone No."></ion-input>\n      </ion-item>\n\n  \n      <ion-item [class.invalid]="!signUpThreeForm.controls.residence.valid  && (signUpThreeForm.controls.residence.touched  || submitAttempt)">\n       <ion-input type="text" formControlName="residence" placeholder="Residence"></ion-input>\n      </ion-item>\n\n        <ion-row>\n          <ion-col>\n               <button  ion-button block outline (click)="prev(1)" color="light" class="login-button">BACK</button>\n          </ion-col>\n\n          <ion-col>\n           <button [disabled]="!signUpThreeForm.valid" type="submit" ion-button block outline (click)="next(3,signUpThreeForm)" color="light" class="login-button">NEXT</button>\n          </ion-col>\n\n         </ion-row>\n    </form>\n\n  </ion-slide>\n\n\n  <ion-slide>\n   \n      <form [formGroup]="signUpFourForm">\n        <ion-item [class.invalid]="!signUpFourForm.controls.password.valid  && (signUpFourForm.controls.password.touched  || submitAttempt)">\n        <ion-input type="password" formControlName="password" placeholder="Password"></ion-input>\n      </ion-item>\n\n   \n\n      <ion-item  [class.invalid]="!signUpFourForm.controls.confirm_password.valid  && (signUpFourForm.controls.confirm_password.touched  || submitAttempt)">\n        <ion-input type="password" formControlName="confirm_password" placeholder="Confirm Password"></ion-input>\n      </ion-item>\n\n      <ion-row>\n          <ion-col>\n               <button  ion-button block outline (click)="prev(2)" color="light" class="login-button">BACK</button>\n          </ion-col>\n\n          <ion-col>\n          <button [disabled]="!signUpFourForm.valid" type="submit" ion-button block outline (click)="register()" color="light" class="login-button">COMPLETE SIGN UP</button>\n          </ion-col>\n\n         </ion-row>\n\n     \n      </form>\n\n  </ion-slide>\n\n\n  </ion-slides>\n</div>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar class="footer">\n    <div (click)="goToSignin()">\n      <span>Have an account? <strong>Sign in here</strong>.</span>\n    </div>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__app_app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_6__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 189:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 189;

/***/ }),

/***/ 234:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/activation/activation.module": [
		767,
		6
	],
	"../pages/home/home.module": [
		768,
		5
	],
	"../pages/register/register.module": [
		769,
		4
	],
	"../pages/setting/setting-list.module": [
		770,
		3
	],
	"../pages/setting/theming/theming.module": [
		771,
		2
	],
	"../pages/side-menu/side-menu.module": [
		772,
		1
	],
	"../pages/slide/slide-walkthrough/slide-walkthrough.module": [
		773,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 234;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConstantVariable; });
var ConstantVariable = {
    //URL API
    APIURL: "#",
    ACTIVATION_FEE: 10,
    COMPANY_NAME: "PESA SASA",
    apiKey: "AIzaSyBRTB6A6BnU29fNgKUIjYG78LPuTH6VoXU",
    authDomain: "pesasasa-a7629.firebaseapp.com",
    databaseURL: "https://pesasasa-a7629.firebaseio.com",
    projectId: "pesasasa-a7629",
    storageBucket: "pesasasa-a7629.appspot.com",
    messagingSenderId: "462605408150"
};
//# sourceMappingURL=constant-variable.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoanListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_loan_service__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_crud_service_statements_service__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__loan_loan_application_loan_application__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__loan_loan_payment_modal_loan_payment_modal__ = __webpack_require__(255);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoanListPage = /** @class */ (function () {
    function LoanListPage(navCtrl, alertCtrl, userService, statementService, loanService, utils) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.userService = userService;
        this.statementService = statementService;
        this.loanService = loanService;
        this.utils = utils;
        this.loans = null;
        this.statements = null;
    }
    LoanListPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.userService.getCurrentUser().then(function (user) {
            if (user != null) {
                _this.user = user;
                _this.getUserLoans(user.id);
            }
        }, function (error) {
            _this.utils.showMessage(error);
        });
    };
    LoanListPage.prototype.applyLoan = function () {
        if (this.loans != null) {
            if (this.checkEligibility()) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__loan_loan_application_loan_application__["a" /* LoanApplicationPage */], {}, { animate: true, direction: 'forward' });
            }
            else {
                this.utils.showDialog('Not Eligible', 'You cannot apply for a new loan because you have unsettled or pending loans');
            }
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__loan_loan_application_loan_application__["a" /* LoanApplicationPage */], {}, { animate: true, direction: 'forward' });
        }
    };
    LoanListPage.prototype.getUserLoans = function (user_id) {
        var _this = this;
        this.utils.createLoader('Loading...');
        var response = this.loanService.getLoans(user_id);
        response.timeout(20000).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.utils.stopLoader();
            var length = Object.keys(data).length;
            if (length > 0) {
                _this.loans = data;
            }
        });
    };
    LoanListPage.prototype.makePayment = function (loan) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__loan_loan_payment_modal_loan_payment_modal__["a" /* LoanPaymentModalPage */], loan);
    };
    LoanListPage.prototype.checkEligibility = function () {
        for (var i = 0; i < this.loans.length; i++) {
            if (this.loans[i].status == 'Pending' || this.loans[i].status == 'Active') {
                return false;
            }
        }
        return true;
    };
    LoanListPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.getUserLoans(_this.user.id);
            refresher.complete();
        }, 2000);
    };
    LoanListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-loan-list',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\loan\loan-list\loan-list.html"*/'<ion-header>\n    <ion-navbar color="primary">\n    <button ion-button menuToggle icon-only>\n      <ion-icon name=\'menu\'></ion-icon>\n    </button>\n    <ion-title>\n      Applied Loans\n    </ion-title>\n\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n\n  <ion-label text-center padding *ngIf="loans==null">\n     No loans applied. <br>Click the \'+\' button below <br>to apply for a loan.\n  </ion-label>\n\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n<ion-card *ngFor="let loan of loans">\n  <ion-card-header  color="primary">\n    Loan Number {{loan.id}}\n  </ion-card-header>\n  <ion-card-content>\n     <p>Due Date: {{loan.dueDate}} </p>\n     <p>Amount Borrowed: Ksh. {{loan.loan_amount}}</p>\n     <p>Amount to pay back: Ksh. {{loan.loan_balance}}</p>\n     <p>Loan Status: {{loan.status}}</p>\n     \n  </ion-card-content>\n\n<ion-row>\n  <ion-col>\n    <button [disabled]="loan.status==\'Pending\' || loan.status == \'Settled\'" ion-button  full round  icon-right color="primary" (click)="makePayment({loanIn: loan})">Make a Payment <ion-icon  name="cash"></ion-icon></button>\n  </ion-col>\n</ion-row>\n\n</ion-card>\n\n <ion-fab bottom right>\n    <button ion-fab  (click)="applyLoan()"><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n\n\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\loan\loan-list\loan-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_crud_service_statements_service__["a" /* StatementService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_loan_service__["a" /* LoanService */],
            __WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */]])
    ], LoanListPage);
    return LoanListPage;
}());

//# sourceMappingURL=loan-list.js.map

/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoanApplicationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_loan_service__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__statements_statements__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoanApplicationPage = /** @class */ (function () {
    function LoanApplicationPage(formBuilder, userService, loanService, navCtrl, alertCtrl, utilities) {
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.loanService = loanService;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.utilities = utilities;
        this.loanApplicationForm = formBuilder.group({
            loan_amount: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(9), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        });
    }
    LoanApplicationPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.userService.getCurrentUser().then(function (user) {
            if (user != null) {
                _this.user = user;
                _this.loan_limit = user.loan_limit;
                _this.utilities.getObject('mpesa_statements').then(function (statements) {
                    if (statements == null) {
                        _this.promptStatements();
                    }
                });
            }
        });
    };
    LoanApplicationPage.prototype.requestLoan = function (details) {
        var _this = this;
        this.utilities.createLoader('Requesting Loan...');
        var response = this.loanService.createLoan(details);
        response.timeout(20000)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.utilities.stopLoader();
            _this.utilities.showDialog('Loan Request Successful', "Funds will be disbursed shortly to your M-PESA");
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
        }, function (error) {
            _this.utilities.stopLoader();
            _this.utilities.showDialog("Error Requesting Loan", error);
        });
    };
    LoanApplicationPage.prototype.confirm = function () {
        var _this = this;
        var loan_amount = this.loanApplicationForm.controls['loan_amount'].value;
        if (loan_amount > this.user.loan_limit) {
            this.utilities.showDialog('Loan Limit Error', "Your Loan request has exceeded your limit");
        }
        else {
            var loan_details_1 = {
                'user': this.user.id,
                'applicationDate': this.utilities.getDate(0),
                'dueDate': this.utilities.getDate(30),
                'loan_amount': loan_amount,
                'status': 'Pending',
                'loan_balance': loan_amount * 1.2
            };
            var confirm_1 = this.alertCtrl.create({
                title: 'Confirm Loan Request',
                message: '<strong> Amount Requested :</strong> Kshs. ' + loan_details_1['loan_amount'] + '<br>' +
                    '<strong> Amount to Repay :</strong> Kshs. ' + loan_details_1['loan_balance'] + '<br>' +
                    '<strong> Application Date:</strong>  ' + loan_details_1['applicationDate'] + '<br>' +
                    '<strong> Due Date :</strong>  ' + loan_details_1['dueDate'] + '<br>',
                buttons: [
                    {
                        text: 'Cancel',
                    },
                    {
                        text: 'Proceed',
                        handler: function () {
                            _this.requestLoan(loan_details_1);
                        }
                    }
                ]
            });
            confirm_1.present();
        }
    };
    LoanApplicationPage.prototype.promptStatements = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Upload your statements',
            message: 'You cannot apply for a new loan because you have not uploaded your MPESA statements. Upload Now',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
                    }
                },
                {
                    text: 'Upload Now',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__statements_statements__["a" /* StatementsPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    LoanApplicationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-loan-application',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\loan\loan-application\loan-application.html"*/'<ion-header >\n  <ion-navbar color="primary">\n     <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Loan Application</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n <ion-grid>\n\n  <ion-card>\n  <ion-card-header  color="primary">\n    What you need to know\n  </ion-card-header>\n  <ion-card-content>\n    <p>* Loans are charged 20% interest.</p>\n    <p>* Pay on time to avoid penalties.</p>\n    <p>* The Loan repayment period is one month.</p>\n    <p>* To acquire a loan, you must have submitted your M-PESA statements.</p>\n     \n  </ion-card-content>\n\n</ion-card>\n\n  \n  <h2 padding-left color="primary">Ready to Borrow ? </h2>\n  <h4 padding>Your loan limit is Ksh. {{ loan_limit }}</h4>\n  \n  <form [formGroup]="loanApplicationForm">\n    <ion-item >\n      <ion-label stacked>How much do you need ? </ion-label>\n      <ion-input  type="number" formControlName="loan_amount" name="loan_amount">\n      </ion-input>\n    </ion-item>\n    <ion-item no-lines>\n     \n    </ion-item>\n\n     <button  ion-button full round  [disabled]="!loanApplicationForm.valid"  (click)="confirm()">Submit</button>\n  </form>\n </ion-grid>\n</ion-content>\n\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\loan\loan-application\loan-application.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_loan_service__["a" /* LoanService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__app_app_utils__["a" /* Utilities */]])
    ], LoanApplicationPage);
    return LoanApplicationPage;
}());

//# sourceMappingURL=loan-application.js.map

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Permissions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Permissions = /** @class */ (function () {
    function Permissions(_platform, _Diagnostic) {
        this._platform = _platform;
        this._Diagnostic = _Diagnostic;
    }
    Permissions.prototype.isAndroid = function () {
        return this._platform.is('android');
    };
    Permissions.prototype.isiOS = function () {
        return this._platform.is('ios');
    };
    Permissions.prototype.isUndefined = function (type) {
        return typeof type === "undefined";
    };
    Permissions.prototype.pluginsAreAvailable = function () {
        return !this.isUndefined(window.plugins);
    };
    Permissions.prototype.requestSMSPermissions = function () {
        var permission = this._Diagnostic.permission;
        return this._Diagnostic.requestRuntimePermission(permission.READ_SMS);
    };
    Permissions.prototype.getSMSPermissionStatus = function () {
        var permission = this._Diagnostic.permission;
        return this._Diagnostic.getPermissionAuthorizationStatus(permission.READ_SMS);
    };
    Permissions.prototype.requestSIMPermissions = function () {
        var permission = this._Diagnostic.permission;
        return this._Diagnostic.requestRuntimePermission(permission.READ_PHONE_STATE);
    };
    Permissions.prototype.getSIMPermissionStatus = function () {
        var permission = this._Diagnostic.permission;
        return this._Diagnostic.getPermissionAuthorizationStatus(permission.READ_PHONE_STATE);
    };
    Permissions.prototype.checkCameraPermissions = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (!_this.pluginsAreAvailable()) {
                alert('Dev: Camera plugin unavailable.');
                resolve(false);
            }
            else if (_this.isiOS()) {
                _this._Diagnostic.getCameraAuthorizationStatus().then(function (status) {
                    if (status == _this._Diagnostic.permissionStatus.GRANTED) {
                        resolve(true);
                    }
                    else if (status == _this._Diagnostic.permissionStatus.DENIED) {
                        resolve(false);
                    }
                    else if (status == _this._Diagnostic.permissionStatus.NOT_REQUESTED || status.toLowerCase() == 'not_determined') {
                        _this._Diagnostic.requestCameraAuthorization().then(function (authorisation) {
                            resolve(authorisation == _this._Diagnostic.permissionStatus.GRANTED);
                        });
                    }
                });
            }
            else if (_this.isAndroid()) {
                _this._Diagnostic.isCameraAuthorized().then(function (authorised) {
                    if (authorised) {
                        resolve(true);
                    }
                    else {
                        _this._Diagnostic.requestCameraAuthorization().then(function (authorisation) {
                            resolve(authorisation == _this._Diagnostic.permissionStatus.GRANTED);
                        });
                    }
                });
            }
        });
    };
    Permissions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], Permissions);
    return Permissions;
}());

//# sourceMappingURL=permissions.service.js.map

/***/ }),

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoanPaymentModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_settlement_service__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_crud_service_mpesa_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_utils__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoanPaymentModalPage = /** @class */ (function () {
    function LoanPaymentModalPage(formBuilder, navParams, userService, mpesaService, alertCtrl, navCtrl, utils, settlementService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.userService = userService;
        this.mpesaService = mpesaService;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.utils = utils;
        this.settlementService = settlementService;
        this.loanpaymentModalForm = formBuilder.group({
            amount: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].maxLength(9), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required])],
        });
        this.userService.getCurrentUser().then(function (user) {
            if (user != null) {
                _this.user = user;
            }
        });
        this.loan = navParams.get('loanIn');
    }
    LoanPaymentModalPage.prototype.confirm = function () {
        var _this = this;
        var amount = this.loanpaymentModalForm.controls['amount'].value;
        if (amount > this.loan.loan_balance) {
            this.utils.showDialog('Amount exceeds Balance', "You cannot pay more than you owe !");
        }
        else {
            var confirm_1 = this.alertCtrl.create({
                title: 'Confirm Loan Repayment',
                message: '<strong> Amount to Pay :</strong> Kshs. ' + amount + '<br>' +
                    '<strong> Balance after payment :</strong> Kshs. ' + (this.loan.loan_balance - amount) + '<br>' +
                    '<strong> Due Date :</strong>  ' + this.loan.dueDate + '<br>',
                buttons: [
                    {
                        text: 'Cancel',
                    },
                    {
                        text: 'Submit',
                        handler: function () {
                            _this.inititatePayment();
                        }
                    }
                ]
            });
            confirm_1.present();
        }
    };
    LoanPaymentModalPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    LoanPaymentModalPage.prototype.inititatePayment = function () {
        var _this = this;
        this.utils.createLoader('Initiating Payment');
        var phone_no = this.user.phone_no.substr(1);
        var paymentDetails = {
            'amount': this.loanpaymentModalForm.controls['amount'].value,
            'phone': "254" + phone_no,
            'reference': "PESA000" + this.user.id,
            'desc': "Loan Settlement",
        };
        var response = this.mpesaService.requestPayment(paymentDetails);
        response.timeout(40000)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.utils.stopLoader();
            if (data.ResponseCode == 0) {
                var transaction_details = {
                    'amount': paymentDetails['amount'],
                    'tracking_id': data.CheckoutRequestID,
                    'reference': _this.loan,
                    'reason': "loan_settlement",
                };
                _this.utils.storeObject('waiting_mpesa', true);
                _this.utils.storeObject('transaction_details', transaction_details);
            }
            else {
                _this.utils.showDialog('MPESA Response', data.ResponseDescription);
            }
        }, function (error) {
            _this.utils.stopLoader();
            _this.utils.showMessage(error);
        });
    };
    LoanPaymentModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-loan-payment-modal',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\loan\loan-payment-modal\loan-payment-modal.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>\n      Loan Payment\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]="loanpaymentModalForm" (ngSubmit)="confirm()">\n    <ion-card> \n    <ion-card-content>\n       <p>Due Date: {{loan.dueDate}} </p>\n       <p>Amount Borrowed: Ksh. {{loan.loan_amount}}</p>\n       <p>Loan Balance: Ksh. {{loan.loan_balance}}</p>\n  \n  \n </ion-card-content>\n  </ion-card>\n   <ion-item>\n        <ion-label stacked> How much do you want to Pay ?</ion-label> \n        <ion-input  type="number"  formControlName="amount"></ion-input>\n  </ion-item>\n\n  <ion-item>\n  <ion-row>\n    <ion-col>\n      <button color="danger" type="button" (click)="goBack()" ion-button full round>Cancel</button>\n    </ion-col>\n    <ion-col>\n         <button color="primary" type="submit" ion-button full round [disabled]="!loanpaymentModalForm.valid">Pay</button>\n    </ion-col>\n  </ion-row>\n\n  </ion-item>\n\n\n\n \n\n</form>\n\n</ion-content>'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\loan\loan-payment-modal\loan-payment-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_crud_service_mpesa_service__["a" /* MpesaService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6__app_app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_3__providers_crud_service_settlement_service__["a" /* SettlementService */]])
    ], LoanPaymentModalPage);
    return LoanPaymentModalPage;
}());

//# sourceMappingURL=loan-payment-modal.js.map

/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettlementService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettlementService = /** @class */ (function () {
    function SettlementService(utils, http) {
        this.utils = utils;
        this.http = http;
        this.SETTLEMENTS_URL = "/settlements/";
        this.request_options = this.utils.getDefaultRequestOptions();
    }
    SettlementService.prototype.getSettlements = function (loan_id) {
        return this.http.get(this.utils.getBaseUrl() + this.SETTLEMENTS_URL + '?loan=' + loan_id);
    };
    SettlementService.prototype.createSettlement = function (data) {
        return this.http.post(this.utils.getBaseUrl() + this.SETTLEMENTS_URL, data, this.request_options);
    };
    SettlementService.prototype.updateSettlement = function (settlement_id, data) {
        return this.http.put(this.utils.getBaseUrl() + this.SETTLEMENTS_URL + settlement_id + "/", data, this.request_options);
    };
    SettlementService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], SettlementService);
    return SettlementService;
}());

//# sourceMappingURL=settlement.service.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_util_alert_service__ = __webpack_require__(258);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfilePage = /** @class */ (function () {
    function ProfilePage(userService, utils, alertService) {
        var _this = this;
        this.userService = userService;
        this.utils = utils;
        this.alertService = alertService;
        this.userService.getCurrentUser().then(function (user) {
            if (user != null) {
                _this.setFormData(user);
            }
        });
    }
    ProfilePage.prototype.setFormData = function (data) {
        this.user_id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.email = data.email;
        this.national_id = data.national_id;
        this.phone_no = data.phone_no;
        this.residence = data.residence;
        this.status = data.status;
        this.loan_limit = data.loan_limit;
        this.device_id = data.device_id;
        this.user_image = "http://api.adorable.io/avatar/" + data.first_name;
    };
    ProfilePage.prototype.saveProfile = function () {
        var _this = this;
        this.utils.createLoader('Updating Profile...');
        var userDetails = {
            'first_name': this.first_name,
            'last_name': this.last_name,
            'email': this.email,
            'national_id': this.national_id,
            'phone_no': this.phone_no,
            'residence': this.residence,
            'status': this.status,
            'loan_limit': this.loan_limit,
            'device_id': this.device_id
        };
        var response = this.userService.updateUser(this.user_id, userDetails);
        response.map(function (res) { return res.json(); }).subscribe(function (data) {
            var user = data;
            _this.userService.storeCurrentUser(user);
            _this.utils.stopLoader();
            _this.utils.showMessage('Profile Updated');
        }, function (error) {
            _this.utils.stopLoader();
            _this.utils.showMessage(error);
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\profile\profile.html"*/'<ion-header>\n    <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Settings</ion-title>\n\n    <ion-buttons end>\n       <button ion-button (click)="saveProfile()">\n      Save\n      </button>\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n</ion-header>\n<ion-content>\n  <form  (ngSubmit)="saveProfile()">\n  <ion-list>\n    <ion-list-header>\n      <ion-avatar item-start>\n         <img [src]="user_image">\n      </ion-avatar>\n      <p class="username">{{first_name}} {{last_name}}</p>\n    </ion-list-header>\n  </ion-list>\n  \n  <ion-list padding>\n    <ion-item no-lines>\n      <ion-label  floating>First Name </ion-label> \n       <ion-input  type="text" [(ngModel)]="first_name" name="first_name"   ></ion-input>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-label  floating>Last Name </ion-label> \n       <ion-input  type="text" [(ngModel)]="last_name" name="last_name"   ></ion-input>\n    </ion-item>\n\n    \n   </ion-list>\n\n  <ion-list>\n    <ion-list-header>\n      Contact info (Visit Help to change details) \n    </ion-list-header>\n    <ion-item>\n      <ion-icon name=\'call\' item-start></ion-icon>\n      <ion-label floating>Phone No. </ion-label>\n      <ion-input disabled="true" [(ngModel)]="phone_no"  name="phone_no"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name=\'mail\' item-start></ion-icon>\n      <ion-label floating>Email  </ion-label>\n      <ion-input disabled="true" [(ngModel)]="email" name="email"></ion-input>\n    </ion-item>\n  </ion-list>\n\n   <ion-list>\n    <ion-list-header>\n      Other Details\n    </ion-list-header>\n    <ion-item>\n      <ion-icon name=\'card\' item-start></ion-icon>\n      <ion-label>National ID</ion-label>\n      <ion-input disabled="true" [(ngModel)]="national_id" name="national_id"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name=\'compass\' item-start></ion-icon>\n      <ion-label>Residence</ion-label>\n      <ion-input [(ngModel)]="residence" name="residence"></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-list-header>\n      Terms and Conditions\n    </ion-list-header>\n    <ion-item>\n      <ion-label>\n        To change the uneditable fields,<br>please contact the administrator.\n      </ion-label>\n     \n    </ion-item>\n  </ion-list>\n</form>\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__app_app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_3__providers_util_alert_service__["a" /* AlertService */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AlertService = /** @class */ (function () {
    function AlertService(alertCtrl) {
        this.alertCtrl = alertCtrl;
    }
    AlertService.prototype.presentAlert = function (title, message) {
        var alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: [
                {
                    text: 'OK'
                }
            ]
        });
        return alert.present();
    };
    AlertService.prototype.presentErrorAlert = function (message) {
        return this.presentAlert('An error has occurred.', message);
    };
    AlertService.prototype.presentAlertWithCallback = function (title, message) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var confirm = _this.alertCtrl.create({
                title: title,
                message: message,
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                            confirm.dismiss().then(function () { return resolve(false); });
                            return false;
                        }
                    }, {
                        text: 'Yes',
                        handler: function () {
                            confirm.dismiss().then(function () { return resolve(true); });
                            return false;
                        }
                    }]
            });
            return confirm.present();
        });
    };
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserService = /** @class */ (function () {
    function UserService(storage, utils, firebaseAuth, http) {
        this.storage = storage;
        this.utils = utils;
        this.firebaseAuth = firebaseAuth;
        this.http = http;
        this.USER_URL = "/app/users/";
        this.request_options = this.utils.getDefaultRequestOptions();
    }
    UserService.prototype.getUser = function (value) {
        return this.http.get(this.utils.getBaseUrl() + this.USER_URL + value + '/');
    };
    UserService.prototype.getUserByEmail = function (email) {
        return this.http.get(this.utils.getBaseUrl() + this.USER_URL + '?email=' + email);
    };
    UserService.prototype.createUser = function (data) {
        return this.http.post(this.utils.getBaseUrl() + this.USER_URL, data, this.request_options);
    };
    UserService.prototype.updateUser = function (user_id, data) {
        return this.http.put(this.utils.getBaseUrl() + this.USER_URL + user_id + "/", data, this.request_options);
    };
    UserService.prototype.storeCurrentUser = function (value) {
        this.storage.set('user', value);
    };
    UserService.prototype.getCurrentUser = function () {
        return this.storage.get('user');
    };
    UserService.prototype.signOutUser = function () {
        this.firebaseAuth.auth.signOut();
        this.storage.clear();
    };
    /*Firebase User Management*/
    UserService.prototype.createUserFB = function (details) {
        return this.firebaseAuth.auth.createUserWithEmailAndPassword(details.email, details.password);
    };
    UserService.prototype.signInUserFB = function (details) {
        return this.firebaseAuth.auth.signInWithEmailAndPassword(details.email, details.password);
    };
    UserService.prototype.deleteUserFB = function () {
        var user = this.firebaseAuth.auth.currentUser;
        return user.delete();
    };
    UserService.prototype.resetPassword = function (email) {
        return this.firebaseAuth.auth.sendPasswordResetEmail(email);
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], UserService);
    return UserService;
}());

//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(408);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_imports__ = __webpack_require__(749);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
 //enableProdMode : make development faster



// this is the magic wandd
Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_3__app_imports__["a" /* ENTRYCOMPONENTS */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__app_imports__["b" /* MODULES */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/activation/activation.module#ActivationPageModule', name: 'ActivationPage', segment: 'activation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting-list.module#SettingListPageModule', name: 'SettingListPage', segment: 'setting-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/theming/theming.module#ThemingPageModule', name: 'ThemingPage', segment: 'theming', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/side-menu/side-menu.module#SideMenuPageModule', name: 'SideMenuPage', segment: 'side-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/slide/slide-walkthrough/slide-walkthrough.module#SlideWalkthroughPageModule', name: 'SlideWalkthroughPage', segment: 'slide-walkthrough', priority: 'low', defaultHistory: [] }
                    ]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_3__app_imports__["a" /* ENTRYCOMPONENTS */]
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_3__app_imports__["d" /* PROVIDERS */],
                __WEBPACK_IMPORTED_MODULE_3__app_imports__["c" /* NATIVES */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_setting__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_onesignal__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_crud_service_mpesa_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_crud_service_loan_service__ = __webpack_require__(83);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = /** @class */ (function () {
    function MyApp(platform, menu, statusBar, splashScreen, global, alertCtrl, menuCtrl, storage, utils, mpesaService, userService, loanService, _notification) {
        var _this = this;
        this.platform = platform;
        this.menu = menu;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.global = global;
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.utils = utils;
        this.mpesaService = mpesaService;
        this.userService = userService;
        this.loanService = loanService;
        this._notification = _notification;
        this.activePage = new __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"]();
        this.isLogin = false;
        this.MENU = {
            DEFAULT: 'menu-components',
            MATERIAL: 'menu-material',
            AVATAR: 'menu-avatar',
        };
        this.initializeApp();
        if (this.platform.is('cordova')) {
            this.initPushNotification();
        }
        //Main Menu
        this.pages = __WEBPACK_IMPORTED_MODULE_4__global_setting__["b" /* PAGES */];
        this.activePage.subscribe(function (selectedPage) {
            _this.pages.map(function (page) {
                page.active = page.title === selectedPage.title;
            });
        });
        this.userService.getCurrentUser().then(function (user) {
            if (user != null) {
                _this.setUserData(user);
                _this.user = user;
            }
        });
        this.userData = {
            'name': '',
            'photo': '',
            'email': '',
        };
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.global.set('theme', '');
            _this.setMenu(_this.MENU.MATERIAL);
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.platform.resume.subscribe(function (result) {
                _this.utils.getObject('waiting_mpesa').then(function (value) {
                    if (value == true) {
                        _this.utils.createLoader('Confirming Payment...');
                        _this.utils.storeObject('waiting_mpesa', false);
                        setTimeout(function () {
                            _this.confirm();
                        }, 3000);
                    }
                });
            });
        });
    };
    MyApp.prototype.setUserData = function (data) {
        this.userData = {
            name: "Welcome " + data.first_name,
            email: data.email,
            photo: "http://api.adorable.io/avatar/" + data.first_name,
            id: data.id,
        };
    };
    MyApp.prototype.openPage = function (pages) {
        this.menu.close();
        this.nav.setRoot(pages.page);
        this.activePage.next(pages);
    };
    //PUSH NOTIFICATION
    MyApp.prototype.initPushNotification = function () {
        var _this = this;
        this._notification.startInit('4292159b-9762-4666-bd76-a17c0a6ed5ba', '462605408150');
        this._notification.inFocusDisplaying(this._notification.OSInFocusDisplayOption.Notification);
        this._notification.setSubscription(true);
        this._notification.getIds().then(function (value) {
            _this.utils.storeObject('notification_id', value.userId);
        });
        this._notification.endInit();
    };
    MyApp.prototype.setMenu = function (menu) {
        var _this = this;
        Object.keys(this.MENU).map(function (k) { return _this.menuCtrl.enable(false, _this.MENU[k]); });
        this.menuCtrl.enable(true, menu);
    };
    MyApp.prototype.exitApp = function () {
        this.userService.signOutUser();
        this.platform.exitApp();
    };
    MyApp.prototype.signOut = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Sign out and Exit ?',
            message: '',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.exitApp();
                    }
                }
            ]
        });
        confirm.present();
    };
    MyApp.prototype.confirm = function () {
        var _this = this;
        var transaction_details;
        this.utils.getObject('transaction_details').then(function (value) {
            transaction_details = value;
            var response = _this.mpesaService.checkTransactionStatus({
                'CheckoutRequestID': transaction_details.tracking_id
            });
            response.timeout(20000)
                .map(function (res) { return res.json(); }).subscribe(function (data) {
                _this.utils.stopLoader();
                _this.utils.storeObject('waiting_mpesa', false);
                console.log(data.ResultCode);
                switch (data.ResultCode) {
                    case "0":
                        if (transaction_details.reason == 'activation') {
                            _this.utils.createLoader('Activating Account...');
                            var user_1 = transaction_details.reference;
                            user_1.status = "Active";
                            var response_1 = _this.userService.updateUser(user_1.id, user_1);
                            response_1.timeout(20000)
                                .map(function (res) { return res.json(); }).subscribe(function (data) {
                                _this.userService.storeCurrentUser(user_1); //sync with local storage
                                _this.utils.stopLoader();
                                _this.utils.showDialog("Payment Successful", 'Your account is now activated.');
                                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */]);
                            }, function (error) {
                                _this.utils.stopLoader();
                                _this.utils.showDialog("Activation Error", error);
                            });
                        }
                        else if (transaction_details.reason == 'loan_settlement') {
                            /*Adjust the loan*/
                            _this.utils.createLoader('Adjusting Loan...');
                            var loan = transaction_details.reference;
                            loan.loan_balance = loan.loan_balance - transaction_details.amount;
                            if (loan.loan_balance == 0) {
                                loan.status = "Settled";
                            }
                            var response_2 = _this.loanService.updateLoan(loan.id, loan);
                            response_2.timeout(20000)
                                .map(function (res) { return res.json(); }).subscribe(function (data) {
                                /*Update user loan limit*/
                                var current_user = _this.user;
                                current_user.loan_limit = current_user.loan_limit * 1.2;
                                var update_response = _this.userService.updateUser(current_user.id, current_user);
                                update_response.subscribe(function (data) {
                                    _this.userService.storeCurrentUser(current_user);
                                    _this.utils.stopLoader();
                                    _this.utils.showDialog("Payment Successful", 'Your Loan has been adjusted successfully');
                                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */]);
                                });
                            }, function (error) {
                                _this.utils.stopLoader();
                                _this.utils.showDialog("Loan Repayment Error", error);
                            });
                        }
                        break;
                    case "1":
                        _this.utils.showDialog('Insufficient Funds', 'Your Account has insufficient funds.Please top up and try again.');
                        break;
                    case "1032":
                        _this.utils.showDialog('Transaction Cancelled', 'Your cancelled the transaction.');
                        break;
                    default:
                        _this.utils.showDialog('Payment Response', data.ResultDesc);
                        break;
                }
            }, function (error) {
                _this.utils.stopLoader();
                console.log(error);
                _this.utils.showMessage('Could not confirm payment');
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\app\app.html"*/'\n<div class="{{global.state[\'theme\']}}">\n  <!--Default Menu-->\n  <ion-menu [content]="content" id="menu-components">\n    <ion-header color="primary">\n      <ion-toolbar>\n        <ion-title> {{ \'MENU\' | translate }} </ion-title>\n      </ion-toolbar>\n    </ion-header>\n    <ion-content>\n      <ion-list>\n        <button menuClose ion-item *ngFor="let p of pages" [class.highlight]="p.active" (click)="openPage(p)">\n        {{ p.title | translate }}\n      </button>\n      <button *ngIf="isLogin" menuClose="left" ion-item detail-none (click)="doLogout()">\n        {{ \'LOGOUT\' | translate}}\n      </button>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n  <!--Side Menu with avatar-->\n  <ion-menu [content]="content" id="menu-avatar">\n    <ion-content>\n      <div #header>\n        <ion-row style="align-items:center;">\n          <ion-col col-3>\n            <img src="assets/icon/icon-email.svg" />\n            <span class="icon-badge">4</span>\n          </ion-col>\n          <ion-col col-6>\n            <img class="user-avatar round" [src]="userData.photo"\n            />\n          </ion-col>\n          <ion-col col-3>\n            <img src="assets/icon/icon-calendar.svg" />\n          </ion-col>\n        </ion-row>\n        <div style="text-align: center;">\n          <p class="name">{{ userData.name  }}</p>\n          <p class="e-mail">{{ userData.email  }}</p>       \n        </div>\n      </div>\n      <ion-list no-lines>\n        <button menuClose ion-item detail-none *ngFor="let p of pages" (click)="openPage(p)">\n          <!--<ion-icon [name]="p.icon" item-left></ion-icon>-->\n          {{ p.title | translate }}\n        </button>\n        <button *ngIf="isLogin" menuClose="left" ion-item detail-none (click)="doLogout()">\n          {{ \'LOGOUT\' | translate}}\n        </button>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n  <!--Material Design Menu-->\n  <ion-menu [content]="content" id="menu-material">\n    <ion-content>\n      <div class="menu-header">\n        <!--material-design-background-->\n        <img class="user-avatar round" [src]="userData.photo"/>\n        <p class="name">{{ userData.name  }}</p>\n        <p class="e-mail">{{ userData.email  }}</p>\n        <p class="e-mail">YOUR ID : [ PESAUSER{{userData.id}} ]</p>\n      </div>\n      <ion-list no-lines>\n        <button menuClose="left" ion-item detail-none *ngFor="let p of pages" (click)="openPage(p)">\n          <ion-icon [name]="p.icon" item-left></ion-icon>\n          {{p.title | translate}}\n        </button>\n        <button  menuClose="left" ion-item detail-none (click)="signOut()">\n            <ion-icon name="power" item-left></ion-icon>\n            Sign Out\n          </button>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n\n  <!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n  <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n</div>\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__global_setting__["a" /* AppState */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_9__app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_11__providers_crud_service_mpesa_service__["a" /* MpesaService */],
            __WEBPACK_IMPORTED_MODULE_12__providers_crud_service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_13__providers_crud_service_loan_service__["a" /* LoanService */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_onesignal__["a" /* OneSignal */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_global_setting__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, alertCtrl, platform, utils) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.utils = utils;
        this.cards = [
            {
                content: 'Repay your loans on time to increase your limit.',
            },
            {
                content: 'Ensure your MPESA statements are uploaded',
            },
            {
                content: 'Borrow as little as Ksh.100',
            }
        ];
        this.pages = __WEBPACK_IMPORTED_MODULE_3__app_global_setting__["b" /* PAGES */];
    }
    HomePage.prototype.showList = function (pages) {
        this.navCtrl.setRoot(pages.page);
    };
    HomePage.prototype.promptActivation = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Contact Admin',
            message: 'Your Account has been blocked.',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.platform.exitApp();
                    }
                }
            ]
        });
        confirm.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title> Pesa Sasa </ion-title>\n  </ion-navbar>\n  \n</ion-header>\n\n<ion-content>\n  <ion-slides autoplay="2000" loop="true" speed="2000">\n    <ion-slide>\n      <div class="slide-item" style="background-image: url(assets/img/home/2.jpg)"></div>\n    </ion-slide>\n    <ion-slide>\n      <div class="slide-item" style="background-image: url(assets/img/home/1.jpg)"></div>\n    </ion-slide>\n    <ion-slide>\n      <div class="slide-item" style="background-image: url(assets/img/home/3.jpg)"></div>\n    </ion-slide>\n  </ion-slides>\n \n  <ion-card *ngFor="let card of cards">\n   \n    <ion-card-content color="primary">\n      {{card.content}}\n    </ion-card-content>\n  </ion-card>\n\n\n  <ion-grid>\n    <ion-row>\n      <ion-col *ngFor="let item of pages; let i = index" col-4 col-sm-4 col-md-6 col-lg-4 col-xl-2 text-center>\n        <button (click)="showList(item)" >\n          <ion-icon color="primary" name="{{ item.icon }}"></ion-icon> <br> {{ item.title }}\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* unused harmony export firebaseConfigEx */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MODULES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return NATIVES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENTRYCOMPONENTS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constant_variable__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_setting__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_swing__ = __webpack_require__(750);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_swing___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_swing__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(760);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__ = __webpack_require__(761);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_crud_service_user_service__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_crud_service_loan_service__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_crud_service_settlement_service__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_crud_service_activation_service__ = __webpack_require__(763);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_crud_service_statements_service__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_crud_service_permissions_service__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_crud_service_mpesa_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_util_toast_service__ = __webpack_require__(764);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_util_alert_service__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_diagnostic__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_sms__ = __webpack_require__(765);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_push__ = __webpack_require__(766);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_splash_screen__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_status_bar__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_android_permissions__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_onesignal__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_register_register__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_login_login__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_home_home__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_activation_activation__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_profile_profile__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_loan_loan_application_loan_application__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_loan_loan_list_loan_list__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_statements_statements__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_loan_loan_payment_modal_loan_payment_modal__ = __webpack_require__(255);
//Used for setting

//used for Firebase
var firebaseConfig = {
    apiKey: __WEBPACK_IMPORTED_MODULE_0__constant_variable__["a" /* ConstantVariable */].apiKey,
    authDomain: __WEBPACK_IMPORTED_MODULE_0__constant_variable__["a" /* ConstantVariable */].authDomain,
    databaseURL: __WEBPACK_IMPORTED_MODULE_0__constant_variable__["a" /* ConstantVariable */].databaseURL,
    projectId: __WEBPACK_IMPORTED_MODULE_0__constant_variable__["a" /* ConstantVariable */].projectId,
    storageBucket: __WEBPACK_IMPORTED_MODULE_0__constant_variable__["a" /* ConstantVariable */].storageBucket,
    messagingSenderId: __WEBPACK_IMPORTED_MODULE_0__constant_variable__["a" /* ConstantVariable */].messagingSenderId
};
//Used for Theming

//MODULE








//PROVIDER











/*NATIVES*/






//ENTRY COMPONENTS









function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var firebaseConfigEx = firebaseConfig;
var MODULES = [
    __WEBPACK_IMPORTED_MODULE_5_angular2_swing__["SwingModule"],
    __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["a" /* BrowserModule */],
    __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
    __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */].forRoot({
        loader: {
            provide: __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["a" /* TranslateLoader */],
            useFactory: (createTranslateLoader),
            deps: [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]]
        }
    }),
    __WEBPACK_IMPORTED_MODULE_8_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
    __WEBPACK_IMPORTED_MODULE_9_angularfire2_auth__["b" /* AngularFireAuthModule */],
    __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
];
var PROVIDERS = [
    __WEBPACK_IMPORTED_MODULE_18__providers_util_alert_service__["a" /* AlertService */],
    __WEBPACK_IMPORTED_MODULE_17__providers_util_toast_service__["a" /* ToastService */],
    __WEBPACK_IMPORTED_MODULE_10__providers_crud_service_user_service__["a" /* UserService */],
    __WEBPACK_IMPORTED_MODULE_11__providers_crud_service_loan_service__["a" /* LoanService */],
    __WEBPACK_IMPORTED_MODULE_13__providers_crud_service_activation_service__["a" /* ActivationService */],
    __WEBPACK_IMPORTED_MODULE_12__providers_crud_service_settlement_service__["a" /* SettlementService */],
    __WEBPACK_IMPORTED_MODULE_14__providers_crud_service_statements_service__["a" /* StatementService */],
    __WEBPACK_IMPORTED_MODULE_16__providers_crud_service_mpesa_service__["a" /* MpesaService */],
    __WEBPACK_IMPORTED_MODULE_15__providers_crud_service_permissions_service__["a" /* Permissions */],
    __WEBPACK_IMPORTED_MODULE_19__app_utils__["a" /* Utilities */],
    __WEBPACK_IMPORTED_MODULE_20__ionic_native_diagnostic__["a" /* Diagnostic */],
    __WEBPACK_IMPORTED_MODULE_1__global_setting__["a" /* AppState */],
];
var NATIVES = [
    __WEBPACK_IMPORTED_MODULE_21__ionic_native_sms__["a" /* SMS */],
    __WEBPACK_IMPORTED_MODULE_22__ionic_native_push__["a" /* Push */],
    __WEBPACK_IMPORTED_MODULE_24__ionic_native_status_bar__["a" /* StatusBar */],
    __WEBPACK_IMPORTED_MODULE_23__ionic_native_splash_screen__["a" /* SplashScreen */],
    __WEBPACK_IMPORTED_MODULE_25__ionic_native_android_permissions__["a" /* AndroidPermissions */],
    __WEBPACK_IMPORTED_MODULE_26__ionic_native_onesignal__["a" /* OneSignal */]
];
var ENTRYCOMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_27__pages_register_register__["a" /* RegisterPage */],
    __WEBPACK_IMPORTED_MODULE_28__pages_login_login__["a" /* LoginPage */],
    __WEBPACK_IMPORTED_MODULE_30__pages_activation_activation__["a" /* ActivationPage */],
    __WEBPACK_IMPORTED_MODULE_31__pages_profile_profile__["a" /* ProfilePage */],
    __WEBPACK_IMPORTED_MODULE_32__pages_loan_loan_application_loan_application__["a" /* LoanApplicationPage */],
    __WEBPACK_IMPORTED_MODULE_35__pages_loan_loan_payment_modal_loan_payment_modal__["a" /* LoanPaymentModalPage */],
    __WEBPACK_IMPORTED_MODULE_33__pages_loan_loan_list_loan_list__["a" /* LoanListPage */],
    __WEBPACK_IMPORTED_MODULE_34__pages_statements_statements__["a" /* StatementsPage */],
    __WEBPACK_IMPORTED_MODULE_29__pages_home_home__["a" /* HomePage */]
];
//# sourceMappingURL=app.imports.js.map

/***/ }),

/***/ 763:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ActivationService = /** @class */ (function () {
    function ActivationService(utils, http) {
        this.utils = utils;
        this.http = http;
        this.ACTIVATIONS_URL = "/app/activations/";
        this.request_options = this.utils.getDefaultRequestOptions();
    }
    ActivationService.prototype.getActivation = function (user_id) {
        return this.http.get(this.utils.getBaseUrl() + this.ACTIVATIONS_URL + '?user=' + user_id);
    };
    ActivationService.prototype.createActivation = function (data) {
        return this.http.post(this.utils.getBaseUrl() + this.ACTIVATIONS_URL, data, this.request_options);
    };
    ActivationService.prototype.updateActivation = function (id, data) {
        return this.http.put(this.utils.getBaseUrl() + this.ACTIVATIONS_URL + id + "/", data, this.request_options);
    };
    ActivationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], ActivationService);
    return ActivationService;
}());

//# sourceMappingURL=activation.service.js.map

/***/ }),

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToastService = /** @class */ (function () {
    function ToastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
    }
    ToastService.prototype.create = function (message, ok, duration) {
        if (ok === void 0) { ok = false; }
        if (duration === void 0) { duration = 2000; }
        if (this.toast) {
            this.toast.dismiss();
        }
        this.toast = this.toastCtrl.create({
            message: message,
            duration: ok ? null : duration,
            position: 'bottom',
            showCloseButton: ok,
            closeButtonText: 'OK'
        });
        this.toast.present();
    };
    ToastService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */]])
    ], ToastService);
    return ToastService;
}());

//# sourceMappingURL=toast.service.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MpesaService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MpesaService = /** @class */ (function () {
    function MpesaService(utils, http) {
        this.utils = utils;
        this.http = http;
        this.MPESA_REQUEST_URL = "/mpesa/stk/process/";
        this.MPESA_STATUS_URL = "/mpesa/stk/status/";
        this.request_options = this.utils.getDefaultRequestOptions();
    }
    MpesaService.prototype.requestPayment = function (details) {
        return this.http.post(this.utils.getBaseUrl() + this.MPESA_REQUEST_URL, details, this.request_options);
    };
    MpesaService.prototype.checkTransactionStatus = function (transaction_details) {
        return this.http.post(this.utils.getBaseUrl() + this.MPESA_STATUS_URL, transaction_details, this.request_options);
    };
    MpesaService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_app_utils__["a" /* Utilities */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], MpesaService);
    return MpesaService;
}());

//# sourceMappingURL=mpesa.service.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoanService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_utils__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoanService = /** @class */ (function () {
    function LoanService(utils, http) {
        this.utils = utils;
        this.http = http;
        this.LOANS_URL = "/app/loans/";
        this.request_options = this.utils.getDefaultRequestOptions();
    }
    LoanService.prototype.getLoans = function (user_id) {
        return this.http.get(this.utils.getBaseUrl() + this.LOANS_URL + '?user=' + user_id);
    };
    LoanService.prototype.createLoan = function (data) {
        return this.http.post(this.utils.getBaseUrl() + this.LOANS_URL, data, this.request_options);
    };
    LoanService.prototype.updateLoan = function (loan_id, data) {
        return this.http.put(this.utils.getBaseUrl() + this.LOANS_URL + loan_id + "/", data, this.request_options);
    };
    LoanService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__app_app_utils__["a" /* Utilities */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], LoanService);
    return LoanService;
}());

//# sourceMappingURL=loan.service.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_crud_service_mpesa_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_utils__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_constant_variable__ = __webpack_require__(238);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ActivationPage = /** @class */ (function () {
    function ActivationPage(navCtrl, alertCtrl, mpesaService, utils) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.mpesaService = mpesaService;
        this.utils = utils;
        this.utils.getObject('user').then(function (value) {
            _this.user = value;
        });
    }
    ActivationPage.prototype.activate = function () {
        var _this = this;
        this.utils.createLoader('Initiating Payment');
        var phone_no = this.user.phone_no.substr(1);
        var paymentDetails = {
            'amount': __WEBPACK_IMPORTED_MODULE_4__app_constant_variable__["a" /* ConstantVariable */].ACTIVATION_FEE,
            'phone': "254" + phone_no,
            'reference': "PESA000" + this.user.id,
            'desc': "Activation Fee",
        };
        var response = this.mpesaService.requestPayment(paymentDetails);
        response.timeout(40000)
            .map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.utils.stopLoader();
            if (data.ResponseCode == 0) {
                var transaction_details = {
                    'amount': __WEBPACK_IMPORTED_MODULE_4__app_constant_variable__["a" /* ConstantVariable */].ACTIVATION_FEE,
                    'tracking_id': data.CheckoutRequestID,
                    'reference': _this.user,
                    'reason': "activation",
                };
                _this.utils.storeObject('waiting_mpesa', true);
                _this.utils.storeObject('transaction_details', transaction_details);
            }
            else {
                _this.utils.showDialog('Pesa Sasa', data.ResponseDescription);
            }
        }, function (error) {
            _this.utils.stopLoader();
            _this.utils.showMessage(error);
        });
    };
    ActivationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-activation',template:/*ion-inline-start:"C:\xampp\htdocs\pesasasa\app\src\pages\activation\activation.html"*/'<ion-header >\n  <ion-navbar color="primary">\n    <ion-title>Activate your Account</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <header>\n    <h1>Simple <span>as</span> 1,2,3</h1>\n    <img src="assets/img/blog/mountain-range-front.png">\n  </header>\n  <main>\n    <p>1. Click the <strong>Pay Now</strong> Button.</p>\n    <p>2. Wait for the M-PESA pop up.</p>\n    <p>3. Complete the M-PESA transaction.</p>\n    <p>4. Once you have paid,your account will be active.</p>\n\n   <button  ion-button (click)="activate()" color="success" block round><strong>PAY NOW</strong></button>\n    <br>\n  </main>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\xampp\htdocs\pesasasa\app\src\pages\activation\activation.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_crud_service_mpesa_service__["a" /* MpesaService */],
            __WEBPACK_IMPORTED_MODULE_3__app_app_utils__["a" /* Utilities */]])
    ], ActivationPage);
    return ActivationPage;
}());

//# sourceMappingURL=activation.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PAGES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppState; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_home_home__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_loan_loan_list_loan_list__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_statements_statements__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_profile_profile__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PAGES = [
    { icon: 'apps', title: 'Home', page: __WEBPACK_IMPORTED_MODULE_0__pages_home_home__["a" /* HomePage */] },
    { icon: 'paper', title: 'My Loans', page: __WEBPACK_IMPORTED_MODULE_1__pages_loan_loan_list_loan_list__["a" /* LoanListPage */] },
    { icon: 'person', title: 'My Profile', page: __WEBPACK_IMPORTED_MODULE_3__pages_profile_profile__["a" /* ProfilePage */] },
    { icon: 'document', title: 'MPESA Statements', page: __WEBPACK_IMPORTED_MODULE_2__pages_statements_statements__["a" /* StatementsPage */] }
];

var AppState = /** @class */ (function () {
    function AppState() {
        this._state = {};
    }
    Object.defineProperty(AppState.prototype, "state", {
        // already return a clone of the current state
        get: function () {
            return this._state = this.clone(this._state);
        },
        // never allow mutation
        set: function (value) {
            throw new Error('do not mutate the `.state` directly');
        },
        enumerable: true,
        configurable: true
    });
    AppState.prototype.get = function (prop) {
        // use our state getter for the clone
        var state = this.state;
        return state.hasOwnProperty(prop) ? state[prop] : state;
    };
    AppState.prototype.set = function (prop, value) {
        // internally mutate our state
        return this._state[prop] = value;
    };
    AppState.prototype.clone = function (object) {
        // simple object clone
        return JSON.parse(JSON.stringify(object));
    };
    AppState = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Injectable"])()
    ], AppState);
    return AppState;
}());

//# sourceMappingURL=global.setting.js.map

/***/ })

},[403]);
//# sourceMappingURL=main.js.map